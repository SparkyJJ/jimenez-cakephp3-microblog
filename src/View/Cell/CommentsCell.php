<?php

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Datasource\Paginator;
/**
 * Comments cell
 */
class CommentsCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
        
    }
    /**
     * Default display method.
     *
     * @return void
     */
    public function display($post_id, $init = false)
    {
        $this->loadModel('Comments');
        // Create a paginator
        $paginator = new Paginator();
        $page_query = $this->request->getQueryParams();
        if($init) {
            unset($page_query['page']);
        }
        // Paginate the model
        $results = $paginator->paginate(
            $this->Comments,
            $page_query,
            [
                // Use a parameterized custom finder.
                'finder' => ['commentFeed' => ['post_id' => $post_id]],
                'limit' => 3
            ]
        );

        $paging = $paginator->getPagingParams() + (array)$this->request->getParam('paging');
        $this->request = $this->request->withParam('paging', $paging);
        $results = $results->isEmpty() ? [] : $results;

        $this->set('comments', $results);


        // $query
        // ->contain([
        //     'Users' => [
        //         'fields' => [
        //             'Users.id',
        //             'Users.username',
        //             'Users.first_name',
        //             'Users.last_name',
        //             'Users.profile_picture',
        //             'Users.profile_picture_dir',
        //             'Users.unique_id'
        //         ]
        //     ],
        //     'Reply' => ['Users']
        // ])
        // ->where(['Comments.post_id' => $id, 'Comments.comment_id IS NULL'])
        // ->order(['Comments.created' => 'DESC'])
        // ->all();

        // $total_comments = $this->Comments->find()->count();

        // $recent_comments = $this->Comments->find()
        //     ->select('comment')
        //     ->order(['created' => 'DESC'])
        //     ->limit(3)
        //     ->toArray();

        // $this->set(['total_comments' => $total_comments, 'recent_comments' => $recent_comments]);
    }
}
