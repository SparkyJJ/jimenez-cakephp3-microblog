<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Search Controller
 *
 *
 * @method \App\Model\Entity\Search[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SearchController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['find']);
    }

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Users.id' => 'asc'
        ]
    ];
    /**
     * Find method
     *
     * @return \Cake\Http\Response|null
     */
    public function find()
    {
        $this->loadModel('Users');

        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        if($this->request->getData('search_item') === null) {
            $search_item = $this->request->getSession()->read('Search.search_item');
        } else {
            $search_item = $this->request->getData('search_item');
            $this->request->getSession()->write('Search.search_item', $search_item);
        }
        $query = $this->Users->find('all', [
            'fields' => [
                'Users.id',
                'Users.username',
                'Users.first_name',
                'Users.last_name',
                'Users.profile_picture',
                'Users.profile_picture_dir',
                'Users.unique_id'
            ],
            'conditions' => [
                'activated' => 1,
                'OR' => [
                    'username LIKE' => '%' . $search_item . '%',
                    'first_name LIKE' => '%' . $search_item . '%',
                    'last_name LIKE' => '%' . $search_item . '%',
                    'CONCAT(first_name, " ", last_name) LIKE' => '%' . $search_item . '%'
                ]
            ]
        ]);
        $users = $this->paginate($query);
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'item.drawer.search.list',
                    'params' => compact('users')
                ]
            ]
        );
    }
}
