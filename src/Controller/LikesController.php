<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\ForbiddenException;


/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['like', 'unlike']);
    }

    /**
     * Like method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function like($post_id)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        if ($this->request->getData('post_id') !== $post_id) {
            throw new ForbiddenException();
        }

        $like = $this->Likes->newEntity();
        if ($this->request->is('post')) {
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            // $like->post_id = $post_id;
            $like->user_id = $this->Auth->user('id');
            if ($this->Likes->save($like)) {
                $this->getLikeCount($like->post_id, true);
            } else {
                throw new ForbiddenException();
            }
        } else {
            throw new ForbiddenException();
        }
    }

    public function unlike($post_id)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        if ($this->request->getData('post_id') !== $post_id) {
            throw new ForbiddenException();
        }

        $this->request->allowMethod(['post', 'delete']);
        $query = $this->Likes->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'post_id' => $this->request->getData('post_id'),
                'user_id' => $this->Auth->user('id')
            ],
        ]);
        $like = $query->first();

        if ($this->Likes->delete($like)) {
            $this->getLikeCount($like->post_id, false);
        } else {
            throw new ForbiddenException();
        }
    }

    /**
     * Get Like Count method
     *
     * @param string|null $id Post id.
     * @param string|null $isLike A value of true means like and false means unlike
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     */
    private function getLikeCount($id, $isLike)
    {
        $this->loadModel('Posts');
        $post = $this->Posts->get($id, [
            'fields' => [
                'id',
                'like_count'
            ]
        ]);

        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.like.button',
                    'params' => compact('post', 'isLike')
                ]
            ]
        );
    }
}
