<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Main Controller
 *
 *
 * @method \App\Model\Entity\Main[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MainController extends AppController
{

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Posts.id' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    /**
     * Index method
     *
     * Main view of the Microblog (includes posts to show in main pages)
     * 
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->setLayout('main');
        $this->loadModel('Posts');
        $this->loadModel('Followers');

        $followingList = $this->Followers->find('followingList', ['user_id' => $this->Auth->user('id')]);
        $query = $this->Posts->find('displayPosts', [
            'auth_id' => $this->Auth->user('id'),
            'followingList' => $followingList
        ]);
        $posts = $this->paginate($query);

        if ($this->request->is('Ajax')) {
            $this->ajaxElementsResponse(
                [
                    'element' => [
                        'view' => '../Main/index',
                        'params' => compact('posts')
                    ]
                ]
            );
        } else {
            $this->set('posts', $posts);
        }
    }
}
