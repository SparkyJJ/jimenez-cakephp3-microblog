<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\ForbiddenException;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    public $paginate = [
        'limit' => 9
    ];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['follow', 'unfollow', 'following', 'followers', 'accept']);
    }
    /**
     * Follow method
     *
     * @return \Cake\Http\Response|null
     */
    public function follow($user_id = null)
    {
        $this->loadModel('Users');
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $follow = $this->Followers->newEntity();
        if ($this->request->is('post')) {
            $follow = $this->Followers->patchEntity($follow, $this->request->getData());
            $follow->user_followee_id = $user_id;
            $follow->user_follower_id = $this->Auth->user('id');
            $profile['user'] = $this->Users->get($user_id);
            if ($this->Followers->save($follow)) {
                $query = $this->Followers->find('all', [
                    'fieldList' => ['id', 'accepted'],
                    'conditions' => [
                        'user_followee_id' => $user_id,
                        'user_follower_id' => $this->Auth->user('id')
                    ],
                ]);
                $profile['user']['isFollowing'] = $query->first();
                $user_count = $this->Users->find('followCount', ['user_id' => $user_id]);
                $authUser_count = $this->Users->find('followCount', ['user_id' => $this->Auth->user('id')]);
            } else {
                throw new ForbiddenException();
            }
            $this->ajaxElementsResponse(
                [
                    'element' => [
                        'view' => 'component.follow.button',
                        'params' => ['user' => $profile['user']]
                    ]
                ],
                compact('user_count', 'authUser_count')
            );
        } else {
            throw new ForbiddenException();
        }
    }

    /**
     * Unfollow method
     *
     * @return \Cake\Http\Response|null
     */
    public function unfollow($user_id = null)
    {
        $this->loadModel('Users');
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->request->allowMethod(['post', 'delete']);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'user_follower_id' => $this->Auth->user('id')
            ],
        ]);
        $temp = $query->first();
        if ($this->Followers->delete($temp)) {
            $profile['user'] = $this->Users->get($user_id);
            $query = $this->Followers->find('all', [
                'fieldList' => ['id'],
                'conditions' => [
                    'user_followee_id' => $user_id,
                    'user_follower_id' => $this->Auth->user('id')
                ],
            ]);
            $profile['user']['isFollowing'] = $query->first();
            $user_count = $this->Users->find('followCount', ['user_id' => $user_id]);
            $authUser_count = $this->Users->find('followCount', ['user_id' => $this->Auth->user('id')]);
        } else {
            throw new ForbiddenException();
        }
        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.follow.button',
                    'params' => ['user' => $profile['user']]
                ]
            ],
            compact('user_count', 'authUser_count')

        );
    }

    /**
     * Accept method
     *
     * @return \Cake\Http\Response|null
     */
    public function accept($user_id = null, $isAccept = false)
    {
        $this->loadModel('Users');
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->request->allowMethod(['post']);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $this->Auth->user('id'),
                'user_follower_id' => $user_id
            ],
        ]);
        $temp = $query->first();

        if ($isAccept) {
            $temp->accepted = 1;
            if ($this->Followers->save($temp)) {
                $authUser_count = $this->Users->find('followCount', ['user_id' => $this->Auth->user('id')]);
            } else {
                throw new ForbiddenException();
            }
        } else {
            if ($this->Followers->delete($temp)) {
                $authUser_count = $this->Users->find('followCount', ['user_id' => $this->Auth->user('id')]);
            } else {
                throw new ForbiddenException();
            }
        }

        $this->request->session()->write('Follow.authUser_count', $authUser_count);
        $this->redirect(['controller' => 'Followers', 'action' => 'requests']);
    }

    /**
     * Get Count method
     *
     * @return \Cake\Http\Response|null
     */

    public function getCount()
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->loadModel('Users');
        $followCount = $this->Users->get($this->Auth->user('id'));
        return $this->ajaxElementsResponse(null, ['follow' => $followCount]);
    }

    /**
     * Following page method
     *
     * @return \Cake\Http\Response|null
     */

    public function following($user_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $type = 'following';
        $name = 'Following';
        $this->loadModel('Followers');
        if (!isset($user_id)) {
            $user_id = $this->Auth->user('id');
        }

        $profile['user'] = $this->Users->get($user_id);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'user_follower_id' => $this->Auth->user('id')
            ],
        ]);
        $profile['user']['isFollowing'] = $query->first();

        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_follower_id' => $user_id,
                'accepted' => 1
            ],
        ])->contain([
            'UserFollowee' => [
                'fields' => [
                    'UserFollowee.id',
                    'UserFollowee.username',
                    'UserFollowee.first_name',
                    'UserFollowee.last_name',
                    'UserFollowee.profile_picture',
                    'UserFollowee.profile_picture_dir',
                    'UserFollowee.unique_id'
                ]
            ],
        ]);
        $follow = $this->paginate($query);
        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Users/follow',
                    'params' => compact('profile', 'follow', 'type', 'name')
                ]
            ]
        );
    }

    /**
     * Followers Page method
     *
     * @return \Cake\Http\Response|null
     */

    public function followers($user_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $type = 'follower';
        $name = 'Followers';
        $this->loadModel('Followers');
        if (!isset($user_id)) {
            $user_id = $this->Auth->user('id');
        }

        $profile['user'] = $this->Users->get($user_id);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'user_follower_id' => $this->Auth->user('id')
            ],
        ]);
        $profile['user']['isFollowing'] = $query->first();

        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'accepted' => 1
            ],
        ])->contain([
            'UserFollower' => [
                'fields' => [
                    'UserFollower.id',
                    'UserFollower.username',
                    'UserFollower.first_name',
                    'UserFollower.last_name',
                    'UserFollower.profile_picture',
                    'UserFollower.profile_picture_dir',
                    'UserFollower.unique_id'
                ]
            ],
        ]);
        $follow = $this->paginate($query);
        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Users/follow',
                    'params' => compact('profile', 'follow', 'type', 'name')
                ]
            ]
        );
    }

    /**
     * Requests Page method
     *
     * @return \Cake\Http\Response|null
     */

    public function requests($user_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $type = 'request';
        $name = 'Requests';
        $this->loadModel('Followers');
        if (!isset($user_id)) {
            $user_id = $this->Auth->user('id');
        }

        $profile['user'] = $this->Users->get($user_id);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'user_follower_id' => $this->Auth->user('id')
            ],
        ]);
        $profile['user']['isFollowing'] = $query->first();

        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'accepted' => 0
            ],
        ])->contain([
            'UserFollower' => [
                'fields' => [
                    'UserFollower.id',
                    'UserFollower.username',
                    'UserFollower.first_name',
                    'UserFollower.last_name',
                    'UserFollower.profile_picture',
                    'UserFollower.profile_picture_dir',
                    'UserFollower.unique_id'
                ]
            ],
        ]);
        $follow = $this->paginate($query);
        $authUser_count = $this->request->getSession()->consume('Follow.authUser_count');
        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Users/follow',
                    'params' => compact('profile', 'follow', 'type', 'name')
                ]
            ],
            compact('authUser_count')
        );
    }
}
