<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['add', 'edit', 'delete']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $post = $this->Posts->get($id, [
            'contain' => [
                'Users' => [
                    'fields' => [
                        'Users.id',
                        'Users.username',
                        'Users.first_name',
                        'Users.last_name',
                        'Users.profile_picture',
                        'Users.profile_picture_dir',
                        'Users.unique_id'
                    ],
                    'Followee' => [
                        'conditions' => ['user_follower_id' => $this->Auth->user('id')]
                    ]
                ],
                'SharePosts' => [
                    'SharePosts',
                    'ShareUsers'
                ],
                'ShareUsers',
                'Likes' => [
                    'conditions' => ['Likes.user_id' => $this->Auth->user('id')],
                    'fields' => [
                        'Likes.post_id',
                        'Likes.user_id'
                    ]
                ]
            ]
        ]);

        //Throw Exception when trying to view a not owned "Only Me" visibility post
        if ($post->visibility === 2 && $post->user_id != $this->Auth->user('id')) {
            throw new ForbiddenException(__('Unable to view post'));
        }

        //Throw Exception when trying to view a user's post which is not followed 
        if ($post->visibility === 1 && empty($post['user']['followee']) && $post->user_id != $this->Auth->user('id')) {
            throw new ForbiddenException(__('Unable to view post'));
        }

        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'item.post',
                    'params' => compact('post')
                ]
            ]
        );
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->user_id = $this->Auth->user('id');
            if ($this->Posts->save($post)) {
                $this->Flash->toast(__('Post submit successful'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
                $post = null;
                return $this->redirect('/main');
            }
            unset($post['image']);
            $errors = $post->getErrors();
            $this->Flash->toast(__('Invalid post. Please try again.'));
        }

        if (isset($post['post_id'])) {
            $this->request->session()->write('SharePost.errors', $errors);
            return $this->redirect(['controller' => 'Posts', 'action' => 'viewShare', $post['post_id'], $post['user_share_id'], true]);
        }

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.post.form',
                    'params' => compact('post', 'errors')
                ]
            ],
            [
                'status' => 'failed'
            ]
        );
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $type = 'edit';
        $errors = null;
        $post = $this->Posts->get($id, [
            'contain' => [
                'Users' => [
                    'fields' => [
                        'Users.id',
                        'Users.username',
                        'Users.first_name',
                        'Users.last_name',
                        'Users.profile_picture',
                        'Users.profile_picture_dir',
                        'Users.unique_id'
                    ]
                ],
                'SharePosts' => [
                    'SharePosts',
                    'ShareUsers'
                ],
                'ShareUsers'
            ]
        ]);
        if ($post->user_id !== $this->Auth->user('id')) {
            throw new ForbiddenException();
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->toast(__('Post has been saved.'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);

                return $this->ajaxElementsResponse(
                    [
                        'element' => [
                            'view' => 'item.post',
                            'params' => compact('post')
                        ]
                    ]
                );
            }
            $errors = $post->getErrors();
            $this->Flash->toast(__('Unable to save post.'));
        }
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.post.form',
                    'params' => compact('post', 'type', 'errors')
                ]
            ],
            [
                'status' => 'failed'
            ]
        );
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($post->user_id != $this->Auth->user('id')) {
            throw new ForbiddenException();
        }

        if ($this->Posts->delete($post)) {
            $this->Flash->toast(__('Your post is deleted successfully.'), [
                'params' => [
                    'status' => 'success'
                ]
            ]);
        } else {
            $this->Flash->toast(__('Unable to delete post.'));
        }
        return $this->redirect('/main');
    }

    /**
     * View share modal method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewShare($postId = null, $userId = null, $isFailed = false)
    {
        $type = 'share';
        if (!$this->request->is('Ajax')) {
            throw new ForbiddenException();
        }

        if (!$postId) {
            throw new NotFoundException(__('Invalid post'));
        }

        if (!$userId) {
            throw new NotFoundException(__('Invalid user'));
        }

        $sharePost = $this->Posts->get($postId, [
            'contain' => [
                'Users' => [
                    'fields' => [
                        'Users.id',
                        'Users.username',
                        'Users.first_name',
                        'Users.last_name',
                        'Users.profile_picture',
                        'Users.profile_picture_dir',
                        'Users.unique_id'
                    ]
                ],
                'SharePosts' => [
                    'OriginalPosts',
                    'OriginalShareUsers'
                ],
                'ShareUsers'
            ]
        ]);

        $post = $this->Posts->newEntity();
        $post['post_id'] = $sharePost['id'];
        $post['user_share_id'] = $sharePost['user_id'];

        if(!isset($sharePost['original_post_id'])) {
            $post['original_post_id'] = $sharePost['id'];
            $post['original_user_share_id'] = $sharePost['user_id'];
        } else {
            $post['original_post_id'] = $sharePost['original_post_id'];
            $post['original_user_share_id'] = $sharePost['original_user_share_id'];
        }

        $errors = $this->request->session()->consume('SharePost.errors');


        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.post.form',
                    'params' => compact('post', 'sharePost', 'type', 'errors')
                ]
            ],
            [
                'status' => $isFailed ? 'failed' : null,
                'type' => $type
            ]
        );
    }
}
