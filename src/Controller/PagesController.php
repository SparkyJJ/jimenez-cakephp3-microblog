<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'view']);
    }
    /**
     * Displays home page
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    public function index($page = null)
    {
        //Redirect to main page if user is already logged in
        if ($this->Auth->user()) {
            $this->redirect('/main');
        }

        $this->viewBuilder()->setLayout('home');
        if(isset($page)) {
            $this->viewBuilder()->setTemplate("/Home/$page");
        } else {
            $this->viewBuilder()->setTemplate("/Home/home");
        }
    }

    /**
     * Home view controller
     * 
     * Changes view of the home pages
     * 
     * @param string $page
     */
    public function view($page = 'home')
    {
        //Redirect to main page if user is already logged in
        if ($this->Auth->user()) {
            $this->redirect('/main');
        }

        if ($this->request->is('Ajax')) {
            $this->ajaxElementsResponse(
                [
                    'element' => [
                        'view' => "../Home/$page"
                    ]
                ]
            );
        } else {
            throw new ForbiddenException();
        }
    }
}
