<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['add', 'delete', 'edit', 'viewEdit', 'viewReply']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($post_id = null, $comment_id = null)
    {
        $this->loadModel('Posts');

        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment->post_id = $post_id;
            $comment->user_id = $this->Auth->user('id');
            $comment->comment_id = $comment_id;

            if ($this->Comments->save($comment)) {
                $this->Flash->toast(__('Comment submit successful'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
            } else {
                $this->Flash->toast(__('Invalid comment. Please try again.'));
            }

            $comments = $this->Comments->find('commentFeed', ['post_id' => $post_id])->limit(3)->all();
            $counters = $this->Posts->find('postCounters', ['post_id' => $post_id]);
        }
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.comment.form',
                    'params' => compact('comment', 'post_id')
                ],
                'feed' => [
                    'view' => 'item.comment.feed',
                    'params' => compact('comments')
                ]
            ],
            [
                'counters' => $counters
            ]
        );
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($post_id = null, $comment_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $comment = $this->Comments->get($comment_id, [
            'contain' => [
                'Users',
                'Reply' => ['Users']
            ]
        ]);

        if ($comment->user_id !== $this->Auth->user('id')) {
            throw new ForbiddenException();
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                $this->Flash->toast(__('Comment edit successful'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
            } else {
                $this->Flash->toast(__('Invalid comment. Please try again.'));
            }
        }
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'item.comment',
                    'params' => compact('comment')
                ]
            ]
        );
    }

    /**
     * Display edit form method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewEdit($comment_id = null, $post_id = null)
    {
        $comment = $this->Comments->get($comment_id);
        if ($comment->user_id !== $this->Auth->user('id')) {
            throw new ForbiddenException();
        }

        $commentForm = [
            'type' => 'edit',
            'comment_id' => $comment_id,
            'post_id' => $post_id,
            'class' => 'edit',
        ];

        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.reply.form',
                    'params' => compact('comment', 'commentForm')
                ]
            ]
        );
    }

    /**
     * Display reply form method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewReply($comment_id = null, $post_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $commentForm = [
            'type' => 'add',
            'comment_id' => $comment_id,
            'post_id' => $post_id,
            'class' => 'reply',
        ];

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.reply.form',
                    'params' => compact('commentForm')
                ]
            ]
        );
    }

    /**
     * Display paginated feed of comments
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function paginatedComments($post_id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->viewBuilder()->setLayout('ajax');
        $this->viewBuilder()->setTemplate('/Ajax/json_cell');

        $this->set('json', []);
        $this->set('id', $post_id);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $post_id = null)
    {
        $this->loadModel('Posts');

        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $this->request->allowMethod(['post', 'delete']);

        $comment = $this->Comments->get($id, [
            'conditions' => ['Comments.post_id' => $post_id]
        ]);

        if ($comment->user_id !== $this->Auth->user('id')) {
            throw new ForbiddenException();
        }

        if ($this->Comments->delete($comment)) {
            $this->Flash->toast(__('Comment has been deleted.'), [
                'params' => [
                    'status' => 'success'
                ]
            ]);
        } else {
            $this->Flash->toast(__('Unable to delete comment.'));
        }

        $comments = $this->Comments->find('commentFeed', ['post_id' => $post_id])->limit(3)->all();
        $comments = $comments->isEmpty() ? [] : $comments;
        $counters = $this->Posts->find('postCounters', ['post_id' => $post_id]);
        
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.comment.form',
                    'params' => compact('comment', 'post_id')
                ],
                'feed' => [
                    'view' => 'item.comment.feed',
                    'params' => compact('comments')
                ]
            ],
            [
                'counters' => $counters
            ]
        );
    }
}
