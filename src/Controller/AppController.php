<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => '/',
            'loginRedirect' => [
                'controller' => 'main',
                'action' => 'index'
            ],

            // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => '/'
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');
        // $this->loadComponent('Csrf');
    }

    /**
     * AJAX Elements Response
     *
     * Creates a JSON View file to return to the AJAX call that contains the element 
     * to display in the view and also contains extra data that might be needed for 
     * the AJAX
     * 
     * @param array|null $elements Array of elements file name and parameters to pass.
     * @param array|null $data Array Extra/Optional Data to pass in json
     * @return JSON View File
     */
    protected function ajaxElementsResponse($elements = null, $data = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->viewBuilder()->setTemplate('/Ajax/json');

        $this->set('json', $data);
        $this->set('elements', $elements);
    }

    /**
     * AJAX Data Response
     *
     * Creates a JSON View file to return to the AJAX call that contains only the data
     * that will be needed for the AJAX
     * 
     * @param array|null $data Array Data to pass in json
     * @return JSON View File
     */
    protected function ajaxDataResponse($data = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->viewBuilder()->setTemplate('/Ajax/json');

        $this->set('json', $data);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //If the user suddenly gets logged out during a request especially an AJAX Request
        //Set header of response 'Requires-Auth' to true|1
        $url = explode('?', $this->referer('/', true))[0];
        if ($this->request->is('Ajax') && $url != '/') {
            if (!$this->Auth->user()) {
                header('Requires-Auth: 1');
            }
        }

        //Resets Auth Session based on DB values
        if ($this->Auth->user()) {
            $this->loadModel('Users');
            $user = $this->Users->get($this->Auth->user('id'), ['contain' => []]);
            $this->Auth->setUser($user);
        }
    }
}
