<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use Exception;
use PharIo\Manifest\InvalidUrlException;
use SebastianBergmann\ObjectEnumerator\InvalidArgumentException;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 5,
        'order' => [
            'Posts.id' => 'desc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['register', 'login', 'activate', 'reactivate']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['edit', 'changePassword']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function profile($user_id = null)
    {
        $this->loadModel('Followers');
        $this->loadModel('Posts');

        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        if (!isset($user_id)) {
            $user_id = $this->Auth->user('id');
        }

        $profile['user'] = $this->Users->get($user_id);
        $query = $this->Followers->find('all', [
            'fieldList' => ['id'],
            'conditions' => [
                'user_followee_id' => $user_id,
                'user_follower_id' => $this->Auth->user('id')
            ],
        ]);
        $profile['user']['isFollowing'] = $query->first();

        $followingList = $this->Followers->find('followingList', ['user_id' => $this->Auth->user('id')]);
        $query = $this->Posts->find('displayPosts', [
            'auth_id' => $this->Auth->user('id'),
            'user_id' => $user_id,
            'followingList' => $followingList,
            'isProfile' => true
        ]);
        
        $posts = $this->paginate($query);

        $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Users/profile',
                    'params' => compact('profile', 'posts')
                ]
            ]
        );
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->unique_id = Security::hash($user->username . $user->email, 'md5', true);
            if ($this->Users->save($user)) {
                // return $this->redirect(['action' => 'index']);
                $activation_link = 'http://' . $_SERVER['SERVER_NAME'] . '/users/activate/' . $user->id . '/' . $user->unique_id;

                $email = new Email();
                $email->viewBuilder()->setTemplate('activate', 'main');
                $email->setEmailFormat('html')
                    ->setTo($user->email)
                    ->setSubject('SparkBlog v2.0 Account Activation')
                    ->setViewVars(['url' => $activation_link])
                    ->send();

                $this->Flash->toast(__('Registration successful. Check your email for activation'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
                return $this->ajaxElementsResponse(
                    [
                        'element' => [
                            'view' => '../Home/home',
                        ]
                    ]
                );
            } else {
                $this->Flash->toast(__('Unable to register user. Check registration fields.'));
            }
        }

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Home/register',
                    'params' => compact('user')
                ]
            ]
        );
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function login()
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if (!$user['activated']) {
                    $this->Flash->toast(__('User account is not yet activated.'), [
                        'params' => [
                            'status' => 'warning',
                            'timeout' => 3000,
                            'actionText' => 'Resend link',
                            'action' => '/users/reactivate/' . $user['unique_id'] . '/' . $user['id']
                        ]
                    ]);
                } else {
                    $this->Auth->setUser($user);
                    $redirectUrl = $this->Auth->redirectUrl();
                    return $this->ajaxDataResponse(['redirectUrl' => $redirectUrl]);
                }
            } else {
                $this->Flash->toast(__('Your username or password is incorrect.'));
            }
        }

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Home/login',
                    'params' => compact('user')
                ]
            ]
        );
    }

    /**
     * Logout method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function logout()
    {
        $this->Flash->toast(__('You are now logout. Please come back again!'), [
            'params' => [
                'status' => 'success'
            ]
        ]);
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $tabView = 'edit';
        $user = $this->Users->get($this->Auth->user('id'), ['contain' => []]);
        unset($user['password']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->toast(__('The user has been saved.'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
                $this->Auth->setUser($user);
            } else {
                $this->Flash->toast(__('The user could not be saved. Please, try again.'));
            }

            return $this->ajaxElementsResponse(
                [
                    'element' => [
                        'view' => 'component.profile.form.edit',
                        'params' => compact('user')
                    ],
                    'drawer' => [
                        'view' => 'component.drawer'
                    ]
                ]
            );
        }

        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => '../Users/settings',
                    'params' => compact('user', 'tabView')
                ]
            ]
        );
    }

    /**
     * Change password
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function changePassword($id = null)
    {
        if (!$this->request->is('Ajax')) {
            throw new MethodNotAllowedException();
        }

        $user = $this->Users->get($this->Auth->user('id'), ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->toast(__('The user has been saved.'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
            } else {
                $this->Flash->toast(__('The user could not be saved. Please, try again.'));
            }
        }
        return $this->ajaxElementsResponse(
            [
                'element' => [
                    'view' => 'component.profile.form.change',
                    'params' => compact('user')
                ]
            ]
        );
    }

    /**
     * Activate method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function activate($id = null, $unique_id = null)
    {
        if (!isset($id) || !isset($unique_id)) {
            $this->Flash->toast(__('Invalid activation link. Please contact the administrator.'));
        } else {

            try {
                $user = $this->Users->get($id, ['contain' => []]);

                if ($id !== strval($user->id) || $unique_id !== $user->unique_id) {
                    $this->Flash->toast(__('No user could be found. Please contact the administrator.'));
                } else {
                    $user->activated = true;
                    if ($this->Users->save($user)) {
                        $this->Flash->toast(__('The user account has been activated. You may now login.'), [
                            'params' => [
                                'status' => 'success'
                            ]
                        ]);
                    } else {
                        $this->Flash->toast(__('Unable to activate user. Please contact the administrator.'));
                    }
                }
            } catch (RecordNotFoundException $e) {
                $this->Flash->toast(__('No user record could be found. Please contact the administrator.'));
            }
        }
        return $this->redirect('/');
    }

    /**
     * Reactivate method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function reactivate($unique_id = null, $id = null)
    {
        if (!isset($id) || !isset($unique_id)) {
            $this->Flash->toast(__('Invalid reactivation link. Please contact the administrator.'));
        } else {

            try {
                $user = $this->Users->get($id, ['contain' => []]);

                if ($id !== strval($user->id) || $unique_id !== $user->unique_id) {
                    $this->Flash->toast(__('No user could be found. Please contact the administrator.'));
                } else {
                    $activation_link = 'http://' . $_SERVER['SERVER_NAME'] . '/users/activate/' . $user->id . '/' . $user->unique_id;

                    $email = new Email();
                    $email->viewBuilder()->setTemplate('activate', 'main');
                    $email->setEmailFormat('html')
                        ->setTo($user->email)
                        ->setSubject('SparkBlog v2.0 Account Activation')
                        ->setViewVars(['url' => $activation_link])
                        ->send();
                }
                $this->Flash->toast(__('Activation Link has been sent. Please check your email.'), [
                    'params' => [
                        'status' => 'success'
                    ]
                ]);
                return $this->redirect('/');
            } catch (RecordNotFoundException $e) {
                $this->Flash->toast(__('No user record could be found. Please contact the administrator.'));
            }
        }
        return $this->redirect('/');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
