<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Filesystem\File;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\BelongsTo $Posts
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\CounterCacheBehavior
 */
class PostsTable extends Table
{
    use SoftDeleteTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CounterCache', [
            'Posts' => ['post_count']
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                'path' => 'webroot{DS}img{DS}{model}{DS}{field}{DS}{field-value:user_id}{DS}{time}{DS}',
                'fields' => [
                    'dir' => 'image_dir'
                ],
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    // $picExt = explode('.', $entity['profile_picture']);
                    return [
                        $path . $entity['image'],
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);

        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('ShareUsers', [
            'className' => 'Users',
            'foreignKey' => 'user_share_id'
        ]);
        $this->belongsTo('SharePosts', [
            'className' => 'Posts',
            'foreignKey' => 'post_id'
        ]);
        $this->belongsTo('OriginalPosts', [
            'className' => 'Posts',
            'foreignKey' => 'original_post_id'
        ]);
        $this->belongsTo('OriginalShareUsers', [
            'className' => 'Users',
            'foreignKey' => 'original_user_share_id'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'post_id'
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'post_id'
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'post_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->setProvider('upload', \Josegonzalez\Upload\Validation\DefaultValidation::class);

        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('body')
            ->maxLength('body', 140)
            ->requirePresence('body', 'create')
            ->notEmptyString('body', 'The post body text field cannot be left empty.');

        $validator
            ->allowEmptyFile('image')
            ->add('image', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => 'There was a problem uploading your photo',
                    'last' => true
                ],
                'extension' => [
                    'rule' => ['extension'],
                    'message' => 'Image uploaded has an invalid file type.',
                    'last' => true
                ],
                'mimeType' => [
                    'rule' => [
                        'mimeType',
                        [
                            'image/png',
                            'image/jpeg',
                            'image/gif',
                            'image/bmp'
                        ]
                    ],
                    'message' => 'Image uploaded has an invalid mime type.',
                    'last' => true
                ],
                'isAboveMaxSize' => [
                    'rule' => ['fileSize', '<=', '2MB'],
                    'message' => 'Image uploaded should be less than 2MB.',
                    'last' => true
                ],
                'validSize' => [
                    'rule' => ['fileSize', '>', '0'],
                    'message' => 'Image uploaded has an invalid file size.',
                    'last' => true
                ]
            ])
            ->add('image', 'fileSuccessfulWrite', [
                'rule' => 'isSuccessfulWrite',
                'message' => 'There was an issue uploading your image.',
                'provider' => 'upload'
            ])
            ->add('image', 'fileCompletedUpload', [
                'rule' => 'isCompletedUpload',
                'message' => 'There was an issue uploading your image.',
                'provider' => 'upload'
            ]);

        $validator
            ->requirePresence('visibility', 'create')
            ->notEmptyString('visibility')
            ->add('visibility', [
                'inList' => [
                    'rule' => ['inList', [0, 1, 2]],
                    'message' => 'Invalid post visibility.',
                    'last' => true
                ]
            ]);

        $validator
            ->integer('user_share_id')
            ->allowEmptyString('user_share_id');

        $validator
            ->integer('post_id')
            ->allowEmptyString('post_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['user_share_id'], 'Users'));
        $rules->add($rules->existsIn(['post_id'], 'Posts'));

        return $rules;
    }

    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $field = 'image';
        $original = $entity->getOriginal($field);
        $originalDir = $entity->getOriginal($field . '_dir');
        if (is_array($original)) {
            $original = $original['name'];
        }

        if ($entity->{$field} != $original && $original != null) {
            $path = ROOT . DS . $originalDir;
            array_map('unlink', glob("$path*.*"));
            rmdir($path);
        }
    }

    public function findPostCounters(Query $query, array $options)
    {
        $id = $options['post_id'];
        return $query->select(['like_count', 'post_count', 'comment_count'])->where(['id' => $id])->first();
    }

    private function visibilityConditions($model, $list, $auth_id, $user_id, $isProfile = false)
    {
        if (empty($list)) {
            $list = [$auth_id];
        }

        if ($isProfile) {
            $conditions = [
                'AND' => [
                    'Posts.user_id' => $user_id,
                    'OR' => [
                        'Posts.user_id' => $auth_id,
                        'Posts.visibility' => 0,
                        'AND' => [
                            'Posts.visibility' => 1,
                            'Posts.user_id IN' => $list
                        ]
                    ],
                    'Posts.deleted IS' => NULL
                ]
            ];
        } elseif ($model === 'SharePosts') {
            $conditions = [
                'OR' => [
                    $model . '.user_id' => $auth_id,
                    $model . '.visibility' => 0,
                    [
                        'AND' => [
                            $model . '.user_id IN' => $list,
                            [
                                'OR' => [
                                    $model . '.visibility' => 1
                                ]
                            ],
                        ]
                    ],
                ]
            ];
        } else {
            $conditions = [
                'OR' => [
                    $model . '.user_id' => $auth_id,
                    [
                        'AND' => [
                            $model . '.user_id IN' => $list,
                            [
                                'OR' => [
                                    $model . '.visibility IN' => [0, 1]
                                ]
                            ],
                        ]
                    ],
                ]
            ];
        }

        return $conditions;
    }

    public function findDisplayPosts(Query $query, array $options)
    {
        $auth_id = $options['auth_id'];
        if (!isset($options['isProfile'])) {
            $isProfile = false;
        } else {
            $isProfile = $options['isProfile'];
        }

        if (!isset($options['followingList'])) {
            $followingList = [];
        } else {
            $followingList = $options['followingList'];
        }

        if (!isset($options['user_id'])) {
            $user_id = $auth_id;
        } else {
            $user_id = $options['user_id'];
        }

        return $query->contain([
            'Users' => [
                'fields' => [
                    'Users.id',
                    'Users.username',
                    'Users.first_name',
                    'Users.last_name',
                    'Users.profile_picture',
                    'Users.profile_picture_dir',
                    'Users.unique_id'
                ]
            ],
            'SharePosts' => [
                'conditions' => $this->visibilityConditions('SharePosts', $followingList, $auth_id, $user_id),
                'OriginalPosts' => [
                    'conditions' => $this->visibilityConditions('SharePosts', $followingList, $auth_id, $user_id),
                ],
                'OriginalShareUsers'
            ],
            'ShareUsers',
            'Likes' => [
                'conditions' => ['Likes.user_id' => $auth_id],
                'fields' => [
                    'Likes.post_id',
                    'Likes.user_id'
                ]
            ],
            'Comments' => [
                'conditions' => ['Comments.comment_id IS NULL'],
                'Users' => [
                    'fields' => [
                        'Users.id',
                        'Users.username',
                        'Users.first_name',
                        'Users.last_name',
                        'Users.profile_picture',
                        'Users.profile_picture_dir',
                        'Users.unique_id'
                    ]
                ],
                'Reply' => ['Users'],
                'sort' => ['Comments.created' => 'DESC'],
            ]
        ])->where($this->visibilityConditions('Posts', $followingList, $auth_id, $user_id, $isProfile));
    }
}
