<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Followers Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Follower get($primaryKey, $options = [])
 * @method \App\Model\Entity\Follower newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Follower[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Follower|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Follower saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Follower patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Follower[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Follower findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\CounterCacheBehavior
 */
class FollowersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('followers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('FollowCount', [
            'UserFollowee' => [
                'follower_count' => [
                    'conditions' => ['Followers.accepted' => 1]
                ],
                'follow_request_count' => [
                    'conditions' => ['Followers.accepted' => 0]
                ]
            ],
            'UserFollower' => [
                'following_count' => [
                    'conditions' => ['Followers.accepted' => 1]
                ]
            ]
        ]);

        $this->belongsTo('UserFollowee', [
            'className' => 'Users',
            'foreignKey' => 'user_followee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UserFollower', [
            'className' => 'Users',
            'foreignKey' => 'user_follower_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('accepted')
            ->allowEmptyString('accepted');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_follower_id'], 'UserFollower'));
        $rules->add($rules->existsIn(['user_followee_id'], 'UserFollowee'));

        return $rules;
    }

    public function findFollowingList(Query $query, array $options)
    {
        $user_id = $options['user_id'];
        return $query
            ->select(['user_followee_id'])
            ->where([
                'Followers.user_follower_id' => $user_id,
                'Followers.accepted' => 1
            ])->formatResults(function(\Cake\Datasource\ResultSetInterface $results) {
                return $results->combine('id', 'user_followee_id');
            })->toArray();
    }

}
