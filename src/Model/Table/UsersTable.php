<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Filesystem\File;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\NotificationsTable&\Cake\ORM\Association\HasMany $Notifications
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'profile_picture' => [
                'path' => 'webroot{DS}img{DS}{model}{DS}{field}{DS}{field-value:unique_id}{DS}',
                'fields' => [
                    'dir' => 'profile_picture_dir'
                ],
                'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp_lg = tempnam(sys_get_temp_dir(), 'upload') . '_lg.' . $extension;
                    $tmp_sm = tempnam(sys_get_temp_dir(), 'upload') . '_sm.' . $extension;

                    // Use the Imagine library to DO THE THING
                    $size_lg = new \Imagine\Image\Box(150, 150);
                    $size_sm = new \Imagine\Image\Box(50, 50);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size_lg, $mode)
                        ->save($tmp_lg);

                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size_sm, $mode)
                        ->save($tmp_sm);

                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp_lg =>  $entity['username'] . '_lg.' . $extension,
                        $tmp_sm => $entity['username'] . '_sm.' . $extension
                    ];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    $picExt = explode('.', $entity['profile_picture']);
                    return [
                        $path . $entity['profile_picture'],
                        // $path . $entity['username'] . '_lg.' . end($picExt),
                        // $path . $entity['username'] . '_sm.' . end($picExt),
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Follower', [
            'className' => 'Followers',
            'foreignKey' => 'user_follower_id',
        ]);
        $this->hasMany('Followee', [
            'className' => 'Followers',
            'foreignKey' => 'user_followee_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->setProvider('upload', \Josegonzalez\Upload\Validation\DefaultValidation::class);

        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmptyString('username', 'Please fill up username field.')
            ->regex('username', '/^[a-zA-Z0-9_]*$/', 'Letter, numbers, and underscores only.')
            ->add('username', [
                'minLength' => [
                    'rule' => ['minLength', 3],
                    'last' => true,
                    'message' => 'Username should have more than 3 characters'
                ]
            ])
            ->add('username', [
                'maxLength' => [
                    'rule' => ['maxLength', 50],
                    'last' => true,
                    'message' => 'Username should have less than 50 characters'
                ]
            ]);

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Please fill up password')
            ->add('password', [
                'minLength' => [
                    'rule' => ['minLength', 8],
                    'last' => true,
                    'message' => 'Password should have a minimum of 8 characters'
                ]
            ])
            ->add('password', [
                'compare' => [
                    'rule' => ['compareWith', 'password_confirmation'],
                    'last' => true,
                    'message' => 'Password does not match password confirmation'
                ]
            ]);

        $validator
            ->scalar('current_password')
            ->notEmptyString('current_password', 'Please fill up current password');

        $validator
            ->scalar('password_confirmation')
            ->requirePresence('password_confirmation', 'create')
            ->notEmptyString('password_confirmation', 'Please fill up password confirmation')
            ->add('password_confirmation', [
                'compare' => [
                    'rule' => ['compareWith', 'password'],
                    'message' => 'Password confirmation does not match password'
                ]
            ]);

        $validator
            ->scalar('first_name')
            ->regex('first_name', '/^[a-zA-Z ]*$/', 'Letters and spaces only for first name.')
            ->add('first_name', [
                'maxLength' => [
                    'rule' => ['maxLength', 40],
                    'last' => true,
                    'message' => 'First name should have less than 40 characters'
                ]
            ])
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name', 'Please fill up first name field.');

        $validator
            ->scalar('last_name')
            ->regex('last_name', '/^[a-zA-Z ]*$/', 'Letters and spaces only for last name.')
            ->add('last_name', [
                'maxLength' => [
                    'rule' => ['maxLength', 40],
                    'last' => true,
                    'message' => 'Last name should have less than 40 characters'
                ]
            ])
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name', 'Please fill up last name field.');

        $validator
            ->requirePresence('gender', 'create')
            ->add('gender', [
                'inList' => [
                    'rule' => ['inList', [0, 1]],
                    'last' => true
                ]
            ])
            ->notEmptyString('gender', 'Please choose a gender');

        $validator
            ->scalar('address')
            ->maxLength('address', 128)
            ->allowEmptyString('address');

        $validator
            ->date('birth_date')
            ->add('birth_date', [
                'isValidDate' => [
                    'rule' => ['isValidDate'],
                    'provider' => 'table',
                    'last' => true,
                    'message' => 'Future dates is not allowed'
                ]
            ])
            ->requirePresence('birth_date', 'create')
            ->notEmptyDate('birth_date', 'Please fill up birth date field');

        $validator
            ->scalar('mobile_no')
            ->add('mobile_no', [
                'regex' => [
                    'rule' => ['custom', '/^$|^09\d{2}-?\d{3}-?\d{4}$/'],
                    'last' => true,
                    'message' => 'Please enter a valid mobile number. <br> Ex. 09XX-XXX-XXXX'
                ]
            ])
            // ->regex('mobile_no', '/^$|^09\d{2}-?\d{3}-?\d{4}$/', 'Please enter a valid mobile number. <br> Ex. 09XX-XXX-XXXX')
            ->maxLength('mobile_no', 16)
            ->allowEmptyString('mobile_no');

        $validator
            ->email('email', false, 'Invalid email format')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Please fill up email field');

        $validator
            ->allowEmptyFile('profile_picture')
            ->add('profile_picture', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => 'There was a problem uploading your photo',
                    'last' => true
                ],
                'isAboveMaxSize' => [
                    'rule' => ['fileSize', '<=', '2MB'],
                    'message' => 'File should be less than 2MB.',
                    'last' => true
                ],
                'validSize' => [
                    'rule' => ['fileSize', '>', '0'],
                    'message' => 'Invalid file size.',
                    'last' => true
                ],
                'extension' => [
                    'rule' => ['extension'],
                    'message' => 'Invalid file type.',
                    'last' => true
                ],
                'mimeType' => [
                    'rule' => [
                        'mimeType',
                        [
                            'image/png',
                            'image/jpeg',
                            'image/gif',
                            'image/bmp'
                        ]
                    ],
                    'message' => 'Invalid mime type.',
                    'last' => true
                ]
            ])
            ->add('profile_picture', 'fileSuccessfulWrite', [
                'rule' => 'isSuccessfulWrite',
                'message' => 'This upload failed',
                'provider' => 'upload'
            ])
            ->add('profile_picture', 'fileCompletedUpload', [
                'rule' => 'isCompletedUpload',
                'message' => 'This file could not be uploaded completely',
                'provider' => 'upload'
            ]);

        // $validator
        //     ->scalar('profile_picture_dir')
        //     ->maxLength('profile_picture_dir', 128)
        //     ->allowEmptyFile('profile_picture_dir');

        $validator
            ->boolean('activated')
            ->allowEmptyString('activated');

        $validator
            ->scalar('unique_id')
            ->maxLength('unique_id', 64)
            ->requirePresence('unique_id', 'create')
            ->notEmptyString('unique_id');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isValidDate($value, $context)
    {
        if (strtotime(date("Y/m/d")) <= strtotime($value)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        $rules->add(
            function ($entity) {
                if (isset($entity->current_password)) {
                    if (!password_verify($entity->current_password, $entity->getOriginal('password'))) {
                        return false;
                    }
                }
                return true;
            },
            'correctPassword',
            [
                'errorField' => 'current_password',
                'message' => 'Invalid password. Please try again.'
            ]
        );

        return $rules;
    }

    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $field = 'profile_picture';

        if (isset($entity->{$field})) {
            $temp = explode('?', $entity->{$field});
            $entity->{$field} = $temp[0] . '?' . time();
        }
    }

    public function afterSave(Event $event, Entity $entity, ArrayObject $options)
    {
        $field = 'profile_picture';
        $original = $entity->getOriginal($field);
        if (is_array($original)) {
            $original = $original['name'];
        }

        if ($entity->{$field} != $original && $original != null) {
            $tempOrig = explode('.', $original);
            $origExt = explode('?', end($tempOrig));
            $tempPic = explode('.', $entity->{$field});
            $picExt = explode('?', end($tempPic));
            $path = ROOT . DS . $entity->profile_picture_dir . $tempOrig[0] . '.' . $origExt[0];
            $file = new File($path);
            if ($file->exists()) {
                $file->delete();
            }
            $path = ROOT . DS . $entity->profile_picture_dir . $entity->username . '_lg.' . $origExt[0];
            $file = new File($path);
            if ($file->exists() && ($origExt[0] != $picExt[0])) {
                $file->delete();
            }
            $path = ROOT . DS . $entity->profile_picture_dir . $entity->username . '_sm.' . $origExt[0];
            $file = new File($path);
            if ($file->exists() && ($origExt[0] != $picExt[0])) {
                $file->delete();
            }
            $file->close();
        }
    }

    public function findFollowCount(Query $query, array $options)
    {
        $id = $options['user_id'];
        return $query->select(['follower_count', 'following_count', 'follow_request_count'])->where(['id' => $id])->first();
    }

}
