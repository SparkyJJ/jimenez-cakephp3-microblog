<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property int $gender
 * @property string|null $address
 * @property \Cake\I18n\FrozenDate $birth_date
 * @property string|null $mobile_no
 * @property string $email
 * @property string $password
 * @property string|null $profile_picture
 * @property string|null $profile_picture_dir
 * @property int|null $follower_count
 * @property int|null $following_count
 * @property int|null $follow_request_count
 * @property bool|null $activated
 * @property string $unique_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool|null $deleted
 * @property \Cake\I18n\FrozenTime|null $deleted_date
 *
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Like[] $likes
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\Post[] $posts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'first_name' => true,
        'last_name' => true,
        'gender' => true,
        'address' => true,
        'birth_date' => true,
        'mobile_no' => true,
        'email' => true,
        'password' => true,
        'current_password' => true,
        'profile_picture' => true,
        'profile_picture_dir' => true,
        'follower_count' => true,
        'following_count' => true,
        'follow_request_count' => true,
        'activated' => true,
        'unique_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'deleted_date' => true,
        'comments' => true,
        'likes' => true,
        'notifications' => true,
        'posts' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    // Add this method
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
}
