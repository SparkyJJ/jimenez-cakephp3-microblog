<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $body
 * @property string|null $image
 * @property string|null $image_dir
 * @property int $visibility
 * @property int|null $user_share_id
 * @property int|null $post_id
 * @property int|null $like_count
 * @property int|null $post_count
 * @property int|null $comment_count
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool|null $deleted
 * @property \Cake\I18n\FrozenTime|null $deleted_date
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Post[] $posts
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Like[] $likes
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'body' => true,
        'image' => true,
        'image_dir' => true,
        'visibility' => true,
        'user_share_id' => true,
        'post_id' => true,
        'original_post_id' => true,
        'like_count' => true,
        'post_count' => true,
        'comment_count' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'deleted_date' => true,
        'user' => true,
        'posts' => true,
        'comments' => true,
        'likes' => true
    ];
}
