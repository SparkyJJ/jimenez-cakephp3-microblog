<div>
    <div class="content">
        <h2>SparkBlog v2.0</h2>
        <p>
            Thanks for signing up to this awesome blog! Activate your account by clicking the button below!
        </p>
        
        <?php echo $this->Html->link('Activate me!', $url); ?>

        <p>Having trouble on clicking the button, copy this link and paste in your browser!</p>
        <a href="<?= h($url) ?>"><?= h($url) ?></a>
    </div>
</div>