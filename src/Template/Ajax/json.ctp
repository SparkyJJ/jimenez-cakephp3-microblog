<?php
if(isset($elements)) {
    foreach($elements as $key => $value) {
        $elementParams = isset($value['params']) ? $value['params'] : [];
        $json['content'][$key] = $this->element($value['view'], $elementParams);
    }
}
echo json_encode($json);