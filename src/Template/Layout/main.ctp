<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('raleway.css') ?>
    <?= $this->Html->css('material.min.css') ?>
    <?= $this->Html->css('icon.css') ?>
    <?= $this->Html->css('general.css') ?>
    <?= $this->Html->css('main.css') ?>
    <?= $this->Html->css('datepicker.min.css') ?>
    <?= $this->Html->css('transition.min.css') ?>
    <?= $this->Html->css('comment.min.css') ?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('material.min.js') ?>
    <?= $this->Html->script('datepicker.min.js') ?>
    <?= $this->Html->script('i18n/datepicker.en.js') ?>
    <?= $this->Html->script('dialog-polyfill.js') ?>
    <?= $this->Html->script('transition.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer">
        <?= $this->element('component.drawer') ?>
        <main class="mdl-layout__content">
            <div class="main-view-loader hide-element" style="height:100%; background-color:#f5f5f5">
                <div class="center-element">
                    <div class="mdl-spinner mdl-js-spinner is-active"></div>
                </div>
            </div>
            <div id="sb-main-view" class="page-content mdl-grid" style="width:100%">
                <!-- Your content goes here -->
                <?= $this->fetch('content') ?>
            </div>
        </main>
        <?= $this->element('component.subdrawer') ?>
    </div>
    <dialog id="general-modal" class="mdl-dialog">
        <?= $this->element('component.post.form') ?>
        <!-- <div class="mdl-dialog__content">&nbsp;</div>
        <div class="mdl-dialog__actions">&nbsp;</div> -->
    </dialog>
    <div id="general-snackbar" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text center-mdl-icon-text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>
    <?= $this->Flash->render() ?>
    <?= $this->Html->script('main.js') ?>
    <?= $this->Html->script('drawer.js') ?>
    <?= $this->Html->script('settings.js') ?>
    <?= $this->Html->script('post.js') ?>
    <?= $this->Html->script('comment.js') ?>
</body>

</html>