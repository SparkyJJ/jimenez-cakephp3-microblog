<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('raleway.css') ?>
    <?= $this->Html->css('material.min.css') ?>
    <?= $this->Html->css('icon.css') ?>
    <?= $this->Html->css('general.css') ?>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->css('datepicker.min.css') ?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('material.min.js') ?>
    <?= $this->Html->script('datepicker.min.js') ?>
    <?= $this->Html->script('i18n/datepicker.en.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <div id="main-container" class="mdl-grid" style="width:100%; min-height:100vh; padding:0px">
        <?= $this->fetch('content') ?>
    </div>
    <div id="general-snackbar" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text center-mdl-icon-text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>
    <?= $this->Flash->render('flash', [
        'element' => 'Flash/toast'
    ]) ?>
    <?= $this->Html->script('home.js') ?>
</body>

</html>