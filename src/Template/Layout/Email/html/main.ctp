<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('raleway.css') ?>
    <?= $this->Html->css('material.min.css') ?>
    <?= $this->Html->css('icon.css') ?>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('material.min.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <?= $this->Flash->render() ?>
    <div id="main-container" class="mdl-grid" style="width:100%; min-height:100vh; padding:0px">
        <?= $this->fetch('content') ?>
    </div>
    <?= $this->Html->script('home.js') ?>
</body>

</html>