<?php
$this->Paginator->setTemplates([
    'nextActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2 sb-comment-paginate" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_forward_ios</i></a>',
    'prevActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2 sb-comment-paginate" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_back_ios</i></a>',
]);
?>
<?php if (!empty($comments)) : ?>
    <div class="sb-comment-feed">
        <div class="mdl-grid" style="padding-bottom: 16px">
            <div class="mdl-cell mdl-cell--1-col" style="margin-top:0px; display: flex; justify-content: center;align-items: center;">
                <?= $this->Paginator->prev()
                    ?>
            </div>
            <div class="mdl-cell mdl-cell--10-col" style="margin-top:0px">
                <div class="ui threaded comments">

                    <?php
                        foreach ($comments as $comment) {
                            echo $this->element('item.comment', ['comment' => $comment]);
                            // echo $this->element('component.comment.form', ['comment' => $comment, 'post_id' => $comment['post_id']]);
                        }
                        // foreach ($post['Comment'] as $row) {
                        //     if (!isset($row['Comment'])) {
                        //         $row['Comment'] = $row;
                        //         $row['User'] = $row['Comment']['User'];
                        //         $row['Reply'] = $row['Comment']['Reply'];
                        //         unset($row['Comment']['Reply']);
                        //         unset($row['Comment']['User']);
                        //     }
                        //     echo $this->element('item.comment', ['comment' => $row]);
                        // }
                        ?>
                </div>
            </div>
            <div class="mdl-cell mdl-cell--1-col" style="margin-top:0px; display: flex; justify-content: center;align-items: center;">
                <?= $this->Paginator->next()
                    ?>

            </div>
        </div>
    </div>
<?php endif; ?>
<?= $this->Flash->render() ?>