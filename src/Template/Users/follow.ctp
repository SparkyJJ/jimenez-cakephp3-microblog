<?= $this->element('item.profile.card', ['user' => $profile['user']]); ?>
<div class="mdl-cell mdl-cell--12-col">
    <h5 style="text-align: center; margin:8px"><?= $name ?></h5>
</div>
<?php
$follow = $follow->isEmpty() ? [] : $follow;
foreach ($follow as $user) {
    echo $this->element('item.user.card', ['user' => $user, 'type' => $type]);
}

?>
<?php if (!empty($follow)) : ?>
    <div class="mdl-cell mdl-cell--12-col">
        <div class="sb-ajax-link__main" style="display: flex; justify-content: center;">
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
        </div>
    </div>
<?php else : ?>
    <div class="mdl-cell mdl-cell--12-col" style="border-top: #ddd solid 1px; border-bottom: #ddd solid 1px">
        <h5 style="text-align: center; margin:8px">No <?= $type ?> could be found.</h5>
    </div>
<?php endif; ?>
<script>
    $(document).ready(function() {
        initializeNewView();
    })
</script>