<?php 
$posts = $posts->isEmpty() ? [] : $posts;
?>
<?= $this->element('item.profile.card', ['user' => $profile['user']]); ?>
<div class="sb-main-feed" <?= empty($posts) ? 'style="width: 100%"' : '' ?>>
    <?php
    foreach ($posts as $post) {
        echo $this->element('item.post', ['post' => $post]);
    }
    ?>
    <br>
    <?php if (!empty($posts)) : ?>
        <div class="sb-ajax-link__main" style="width: 100%; display: flex; justify-content: center;">
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
        </div>
    <?php else : ?>
        <div class="mdl-cell mdl-cell--12-col" style="border-top: #ddd solid 1px; border-bottom: #ddd solid 1px">
            <h5 style="text-align: center; margin:8px">No posts available.</h5>
        </div>
    <?php endif; ?>
    <script>
        $(document).ready(function() {
            initializeNewView();
        })
    </script>
</div>