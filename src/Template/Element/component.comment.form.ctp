<div class="image-multext-container sb-comment-form-segment sb-ajax-link__main" style="overflow:visible">
    <?= $this->element('component.loader') ?>
    <?= $this->element('SubElement/avatar', ['size' => 'sm']) ?>

    <div class="image-multext-multext sb-comment-form-container" style="margin-right: 0px">
        <?php
        // if (!isset($comment)) {
        $comment = null;
        // }
        echo $this->Form->create($comment, [
            'url' => ['controller' => 'Comments', 'action' => 'add', (isset($comment['post_id'])) ? $comment['post_id'] : $post_id],
            'class' => 'sb-comment-form',
            'novalidate' => true,
            'style' => 'display: flex'
        ]);
        echo $this->Form->control('comment', [
            'templateVars' => [
                'containerStyle' => 'style="width: 100%"'
            ],
            'error' => false
        ]);
        echo $this->Form->control('user_id', ['type' => 'hidden']);
        echo $this->Form->control('post_id', ['type' => 'hidden']);
        ?>
        <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab sb-color2 mdl-color-text--white sb-comment-form-button" style="margin: 8px 0 8px 16px;">
            <i class="material-icons">send</i>
        </button>
        <?php
        echo $this->Form->end();
        ?>

    </div>
    <?= $this->Flash->render() ?>
</div>