<div class="image-multext-container sb-reply-container" style="margin: 16px 0px">
    <?= $this->element('component.loader') ?>

    <?php
    if (!isset($comment)) {
        $comment = null;
    }
    echo $this->Form->create($comment, [
        'url' => ['controller' => 'Comments', 'action' => $commentForm['type'], $commentForm['post_id'], $commentForm['comment_id']],
        'class' => 'sb-comment-' . $commentForm['class'] . '-form',
        'novalidate' => true,
        'style' => 'display: flex;'
    ]);
    echo $this->Form->control('comment', [
        'templateVars' => [
            'containerStyle' => 'style="width: 100%"'
        ],
        'templates' => [
            'inputContainer' => '<div class="mdl-textfield mdl-js-textfield {{containerClass}}" {{containerStyle}}>{{content}}</div>',
        ],
        'error' => false
    ]);
    echo $this->Form->control('user_id', ['type' => 'hidden']);
    echo $this->Form->control('post_id', ['type' => 'hidden']);
    ?>
    <!-- FAB button with ripple -->
    <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab" style="margin-left: 16px">
        <i class="material-icons">send</i>
    </button>
    <!-- <button class="mdl-button mdl-js-button mdl-button--icon  mdl-js-ripple-effect sb-color2 mdl-color-text--white sb-comment-form-button" style="float: right; height:50px; width: 50px;">
        <i class="material-icons">send</i>
    </button> -->
    <?php
    echo $this->Form->end();
    ?>

    <?= $this->Flash->render() ?>
</div>