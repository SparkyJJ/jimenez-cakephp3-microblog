<?php
$authUser = $this->request->getSession()->read('Auth.User');
?>
<div class="comment post-comment" data-comment-id="<?= $comment['id'] ?>" style="margin-top: 16px;">
    <div class="avatar sb-ajax-link__main">
        <?= $this->element('SubElement/avatar', ['avatar' => $comment['user'], 'size' => 'sm']) ?>
    </div>
    <div class="content sb-ajax-link__main">
        <?php
        echo $this->Html->link(
            $comment['user']['first_name'] . ' ' . $comment['user']['last_name'],
            ['controller' => 'Users', 'action' => 'profile', $comment['user']['id']],
            ['class' => 'mdl-color-text--teal-800']
        );
        ?>
        <div class="metadata">
            @<?= h($comment['user']['username']) ?>
        </div>
        <div class="metadata">
            <span class="date">
                <?= $comment['created']->format('F d, Y g:i A') ?>
            </span>
        </div>
        <div class="text comment-text">
            <p><?= nl2br(h($comment['comment'])) ?></p>
        </div>
        <div class="actions">
            <?php if (!isset($comment['comment_id'])) : ?>
                <?php
                    echo $this->Form->postLink(
                        'Reply',
                        ['controller' => 'comments', 'action' => 'viewReply', $comment['id'], $comment['post_id']],
                        ['class' => 'mdl-button mdl-js-button mdl-js-ripple-effect sb-comment-action-button mdl-color-text--teal-900 sb-comment-reply sb-postLink']
                    );
                    ?>
            <?php endif; ?>
            <?php if ($comment['user_id'] === $authUser['id']) : ?>
                <?php
                    echo $this->Form->postLink(
                        'Edit',
                        ['controller' => 'comments', 'action' => 'viewEdit', $comment['id'], $comment['post_id']],
                        ['class' => 'mdl-button mdl-js-button mdl-js-ripple-effect sb-comment-action-button mdl-color-text--teal-900 sb-comment-edit sb-postLink']
                    );
                    echo $this->Form->postLink(
                        'Delete',
                        ['controller' => 'comments', 'action' => 'delete', $comment['id'], $comment['post_id']],
                        ['class' => 'mdl-button mdl-js-button mdl-js-ripple-effect sb-comment-action-button mdl-color-text--teal-900 sb-comment-delete sb-postLink']
                    );
                    ?>
            <?php endif; ?>
        </div>
    </div>
    <?php if (!empty($comment['reply'])) : ?>
        <div class="comments reply-comments" style="padding: 2em 0 2em 2.25em;">
            <?php
                foreach ($comment['reply'] as $row) {
                    echo $this->element('item.comment', ['comment' => $row]);
                }
                ?>
        </div>
    <?php endif; ?>
</div>
<?= $this->Flash->render() ?>