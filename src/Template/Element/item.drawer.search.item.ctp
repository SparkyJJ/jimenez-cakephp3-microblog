<li class="mdl-list__item mdl-list__item--two-line" data-user-id="<?= $user['id'] ?>">
    <span class="mdl-list__item-primary-content mdl-color-text--white" style="font-size: 12px">
        <?php
        if (isset($user['profile_picture'])) {
            $picExt = explode('.', $user['profile_picture']);
            $dir = 'Users/profile_picture/' . $user['unique_id'] . '/' . $user['username'] . "_sm." . end($picExt);
        } else {
            $dir = 'Users/profile_picture/default/default.png';
        }

        $imageConfig = [];

        echo $this->Html->image(
            $dir,
            [
                'class' => 'mdl-list__item-avatar',
            ]
        );
        ?>
        <!-- <i class="material-icons mdl-list__item-avatar">person</i> -->
        <span><?= $user['first_name'] . ' ' . $user['last_name'] ?></span>
        <span class="mdl-list__item-sub-title" style="color: #aaa;font-size: 12px">@<?= $user['username'] ?></span>
    </span>
    <a href="/users/profile/<?= $user['id'] ?>" style="display: none"></a>
</li>