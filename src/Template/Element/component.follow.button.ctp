<?php
if (!isset($user['isFollowing'])) {
    echo $this->Form->postLink(
        '<span class="mdl-chip__text">Follow</span>',
        ['controller' => 'Followers', 'action' => 'follow', $user['id'], $user['username']],
        [
            'class' => 'mdl-chip sb-color2 mdl-color-text--white sb-follow-button sb-ajax-exclude',
            'escape' => false
        ]
    );
} else {
    $label = 'Unfollow';
    if (!$user['isFollowing']['accepted']) {
        $label = 'Pending';
    }
    echo $this->Form->postLink(
        "<span class='mdl-chip__text'>$label</span>",
        ['controller' => 'Followers', 'action' => 'unfollow', $user['id'], $user['username']],
        [
            'class' => 'mdl-chip sb-color2 mdl-color-text--white sb-follow-button sb-ajax-exclude',
            'escape' => false
        ]
    );
}
