<div id="sb-profile-change-container" class="mdl-card__supporting-text mdl-cell mdl-cell--12-col" style="padding-top: 0px">
    <div class="general-loader hide-element">
        <div class="center-element">
            <div class="mdl-spinner mdl-js-spinner is-active"></div>
        </div>
    </div>
    <?php
    if (!isset($user)) {
        $user = null;
    }
    echo $this->Form->create($user, [
        'url' => ['controller' => 'Users', 'action' => 'changePassword'],
        'type' => 'post',
        'novalidate' => true,
        'error' => false
    ]);
    echo $this->Form->control('current_password', [
        'type' => 'password',
        'templateVars' => ['containerClass' => 'mdl-cell mdl-cell--12-col']
    ]);
    echo $this->Form->control('password', [
        'type' => 'password',
        'templateVars' => ['containerClass' => 'mdl-cell mdl-cell--12-col']
    ]);
    echo $this->Form->control('password_confirmation', [
        'type' => 'password',
        'templateVars' => ['containerClass' => 'mdl-cell mdl-cell--12-col']
    ]);
    echo $this->Form->button('Submit', [
        'type' => 'submit',
        'templateVars' => [
            'buttonClass' => 'mdl-color--teal-400 mdl-color-text--white mdl-cell mdl-cell--12-col'
        ],
        'style' => 'margin-top: 20px'
    ]);
    echo $this->Form->end();
    ?>
    <?= $this->Flash->render() ?>
</div>