<?php
$this->Paginator->setTemplates([
    'nextActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2 sb-search-paginate" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_forward_ios</i></a>',
    'prevActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2 sb-search-paginate" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_back_ios</i></a>',
]);
?>
<ul class="mdl-list sb-search-list-container">
    <?php
    $users = $users->isEmpty() ? [] : $users;

    foreach ($users as $user) {
        echo $this->element('item.drawer.search.item', ['user' => $user]);
    }
    ?>
    <?php if (!empty($users)) : ?>
        <div class="mdl-cell mdl-cell--12-col">
            <div style="display: flex; justify-content: center;">
                <?= $this->Paginator->prev() ?>
                <?= $this->Paginator->next() ?>
            </div>
        </div>
    <?php else : ?>
        <div class="mdl-cell mdl-cell--12-col" style="text-align: center">
            <span class="mdl-color-text--white">No results.</span>
        </div>
    <?php endif; ?>

</ul>