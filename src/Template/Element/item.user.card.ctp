<?php
$authUser = $this->request->getSession()->read('Auth.User');
$rand = rand();
if ($type === 'request' || $type === 'follower') {
    $user = $user['user_follower'];
} else {
    $user = $user['user_followee'];
}
?>
<div class="sb-item-user-card mdl-card mdl-grid mdl-cell mdl-cell--4-col mdl-shadow--2dp" style="padding: 16px; justify-content: center;">
    <?= $this->element('component.loader') ?>
    <?php
    echo $this->element('SubElement/avatar', [
        'avatar' => $user,
        'externalConfig' => [
            'style' => [
                'width: 100%;',
                'border-radius: 50%;',
                'margin: 16px 32px;'
            ]
        ],
        'size' => 'lg'
    ])
    ?>
    <span class="sb-ajax-link__main" style="margin-top: 10px;word-wrap: break-word; text-align: center"><a href="/users/profile/<?= $user['id'] ?>"><?= $user['first_name'] . ' ' . $user['last_name'] ?></a>
        <br>
        <span class="sb-ajax-link__main" style="margin-top: 10px;word-wrap: break-word; font-size: smaller; color: #999"><a href="/users/profile/<?= $user['id'] ?>">@<?= $user['username'] ?></a></span>
    </span>
    <?php if ($type === 'request') : ?>
        <div class="mdl-cell mdl-cell--12-col" style="width: 100%; margin: 0px">
            <?php
                echo $this->Form->postLink(
                    'Accept',
                    ['controller' => 'Followers', 'action' => 'accept', $user['id'], true],
                    [
                        'class' => 'mdl-cell mdl-cell--6-col mdl-button mdl-button--raised mdl-color--green-400 mdl-color-text--white sb-postLink sb-user-request-action',
                        'escape' => false
                    ]
                );
                echo $this->Form->postLink(
                    'Decline',
                    ['controller' => 'Followers', 'action' => 'accept', $user['id'], false],
                    [
                        'class' => 'mdl-cell mdl-cell--6-col mdl-button mdl-button--raised mdl-color--red-400 mdl-color-text--white sb-postLink sb-user-request-action',
                        'escape' => false
                    ]
                );
                ?>
        </div>
    <?php endif; ?>
</div>

<?= $this->Flash->render() ?>