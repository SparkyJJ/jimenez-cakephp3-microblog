<div id="sb-profile-edit-container" class="mdl-card__supporting-text mdl-cell mdl-cell--12-col" style="padding-top: 0px">
    <div class="general-loader hide-element">
        <div class="center-element">
            <div class="mdl-spinner mdl-js-spinner is-active"></div>
        </div>
    </div>
    <?php
    if (!isset($user)) {
        $user = null;
    }
    echo $this->Form->create($user, [
        'url' => ['controller' => 'Users', 'action' => 'edit'],
        'type' => 'file',
        'novalidate' => true,
        'error' => false
    ]);
    echo $this->Form->control('first_name', ['templateVars' => ['containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet']]);
    echo $this->Form->control('last_name', ['templateVars' => ['containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet']]);
    echo $this->Form->control('birth_date', [
        'type' => 'text',
        'templates' => [
            'label' => '<label{{attrs}}>{{text}}</label>'
        ],
        'templateVars' => [
            'containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet',
            'inputClass' => 'datepicker-here'
        ],
        'data-language' => 'en',
        'data-date-format' => 'yyyy/mm/dd',
        'autocomplete' => 'off'
    ]);
    echo $this->Form->control('gender', [
        'type' => 'radio',
        'options' => ['Male', 'Female'],
        'templates' => [
            'label' => '<label>{{text}}</label><br>',
            'inputContainerError' => '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-invalid {{containerClass}}">{{content}}<span class="mdl-textfield__error mdl-cell mdl-cell--12-col {{type}}{{required}} error">{{error}}</span></div>',
        ],
        'templateVars' => ['containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet text-center']
    ]);
    echo $this->Form->control('address', ['templateVars' => ['containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet']]);
    echo $this->Form->control('mobile_no', [
        'templateVars' => [
            'containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet'
        ],
        'error' => ['escape' => false]
    ]);
    ?>
    <div class="mdl-textfield mdl-cell mdl-cell--3-col">Profile Picture: </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--file mdl-cell mdl-cell--6-col <?= (isset($user) && !empty($user->getError('profile_picture'))) ? 'is-invalid' : '' ?>">
        <input class="mdl-textfield__input" placeholder="File" type="text" id="uploadFile" readonly />
        <?php
        echo $this->Form->control('profile_picture', [
            'type' => 'file',
            'class' => 'sb-profile-edit-container__file-upload',
            'templates' => [
                'label' => '<i class="material-icons">attach_file</i>',
                'inputContainer' => '<div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">{{label}}{{content}}</div>',
                'inputContainerError' => '<span class="mdl-textfield__error {{type}}{{required}} error">{{error}}</span><div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">{{label}}{{content}}</div>'
            ],
            'templateVars' => ['containerClass' => 'mdl-cell mdl-cell--6-col-desktop mdl-cell--12-col-tablet']
        ]);
        ?>
    </div>
    <div class="mdl-textfield mdl-cell mdl-cell--2-col mdl-cell--3-offset-tablet mdl-cell--1-offset-phone" id="sb-profile-edit-container__preview-image">
    </div>
    <?php
    echo $this->Form->button('Submit', [
        'type' => 'submit',
        'templateVars' => [
            'buttonClass' => 'mdl-color--teal-400 mdl-color-text--white mdl-cell mdl-cell--12-col'
        ],
        'style' => 'margin-top: 20px'
    ]);
    echo $this->Form->end();
    ?>
    <?= $this->Flash->render() ?>
    <script>
        $('#birth-date').datepicker({
            maxDate: new Date()
        })
    </script>
</div>