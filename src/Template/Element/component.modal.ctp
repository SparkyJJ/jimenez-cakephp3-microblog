<h4 class="mdl-dialog__title mdl-color-text--yellow-800"><i class="material-icons" style="font-size: 64px">warning</i></h4>
<div class="mdl-dialog__content">
    <h5 style="margin:0px">
        Are you sure you want to delete this post?
    </h5>
</div>
<div class="mdl-dialog__actions">
    <button type="button" class="mdl-button mdl-color-text--green-800">Agree</button>
    <button type="button" class="mdl-button close mdl-color-text--red-800">Disagree</button>
</div>