<div id="sb-main-drawer" class="mdl-layout__drawer sb-color1" style="border:none; box-shadow: 8px 0px 8px #bbb">
    <span class="mdl-layout-title mdl-color-text--white">SparkBlog v2.0</span>
    <nav class="mdl-navigation sb-ajax-link__main" style="padding: 0px">
        <a class="mdl-navigation__link sb-color2" style="padding: 16px 40px; text-align:center;" href="">
            <?php
            $drawerUser = $this->request->getSession()->read('Auth.User');
            echo $this->element('SubElement/avatar', [
                'avatar' => $drawerUser,
                'externalConfig' => [
                    'style' => [
                        'width: 100%;',
                        'border-radius: 50%;',
                        'margin-bottom: 10px;'
                    ]
                ],
                'size' => 'lg'
            ])
            ?>
            <span style="margin-top: 10px;word-wrap: break-word"><?= h($drawerUser['first_name']) . ' ' . h($drawerUser['last_name']) ?></span>
            <br>
            <span style="margin-top: 10px;word-wrap: break-word; font-size: smaller; color: #ddd">@<?= h($drawerUser['username']) ?></span>
        </a>
        <a class="mdl-navigation__link" href="/main/index">Home</a>
        <a class="mdl-navigation__link" href="/users/profile">Profile</a>
        <a class="mdl-navigation__link" href="/followers/followers"><span class="mdl-badge sb-drawer-follower-count" data-badge="<?= $drawerUser['follower_count'] ?>">Followers</span></a>
        <a class="mdl-navigation__link" href="/followers/following"><span class="mdl-badge sb-drawer-following-count" data-badge="<?= $drawerUser['following_count'] ?>">Following</span></a>
        <a class="mdl-navigation__link" href="/followers/requests"><span class="mdl-badge sb-drawer-requests-count" data-badge="<?= $drawerUser['follow_request_count'] ?>">Requests</span></a>
        <a class="mdl-navigation__link" href="/users/settings">Account Settings</a>
        <a class="mdl-navigation__link sb-ajax-exclude" href="/logout">Log Out</a>
    </nav>
</div>