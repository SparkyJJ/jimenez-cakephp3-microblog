<div class="mdl-layout__subdrawer-button"><i class="material-icons">arrow_back_ios</i></div>
<div class="mdl-layout__subdrawer sb-color1" style="border:none; box-shadow: -8px 0px 8px #bbb;">
    <!-- <form class="sb-color2" style="padding:16px" action="#"> -->
    <?php
    if (!isset($search)) {
        $search = null;
    }
    echo $this->Form->create($search, [
        'url' => ['controller' => 'Search', 'action' => 'find'],
        'novalidate' => true,
        'class' => 'sb-color2 sb-search-bar',
        'style' => 'padding: 16px; display: flex;'
    ]);

    echo $this->Form->control('search_item', [
        'templates' => [
            'inputContainer' => '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">{{content}}</div>',
            'input' => '<input type="{{type}}" class="mdl-textfield__input  mdl-color-text--white" style="border-bottom: white solid 1px" name="{{name}}"{{attrs}}/>',
            'label' => '<label class="mdl-textfield__label mdl-color-text--white"{{attrs}}>Search...</label>',
        ],
        'autocomplete' => 'off'
    ]);
    ?>
    <!-- FAB button with ripple -->
    <button type="submit" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--mini-fab" style="margin-left: 16px; background-color: #ddd">
        <i class="material-icons">send</i>
    </button>
    <?php
    echo $this->Form->end();
    ?>
    <!-- </form> -->
</div>
<div class="mdl-layout__subobfuscator"></div>