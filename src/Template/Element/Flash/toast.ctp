<?php
if (isset($params['status'])) {
    switch ($params['status']) {
        case 'error':
            $snackbar_icon = '<i class="material-icons mdl-color-text--red-500">error_outline</i>&nbsp;';
            break;
        case 'success':
            $snackbar_icon = '<i class="material-icons mdl-color-text--green-500">check_circle_outline</i>&nbsp;';
            break;
        case 'warning':
            $snackbar_icon = '<i class="material-icons mdl-color-text--yellow-500">warning</i>&nbsp;';
            break;
    }
} else {
    $snackbar_icon = '<i class="material-icons mdl-color-text--red-500">error_outline</i>&nbsp;';
}
?>

    <script>
        r(function() {
            let toast_data = {
                message: '<?= $snackbar_icon . $message ?>',
                timeout: <?= isset($params['timeout']) ? $params['timeout'] : 2000 ?>,
                <?php if (isset($params['actionText'])) : ?>
                    actionHandler: function() {
                        <?php if(isset($params['actionHandler'])): ?>
                        <?= $params['actionHandler'] ?>
                        <?php endif; ?>
                        <?php if(isset($params['action'])): ?>
                        window.location.replace('<?= $params['action'] ?>');
                        <?php endif; ?>
                    },
                    actionText: '<?= $params['actionText'] ?>'
                <?php endif; ?>
            };
            $('#general-snackbar')[0].MaterialSnackbar.showSnackbar(toast_data);
        });

        function r(f) {
            /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
        }
    </script>