    <?php
    if (!isset($type)) {
        $type = 'add';
    }
    if ($type === 'add') {
        $postForm = [
            'title' => 'CREATE POST',
            'buttonName' => 'POST',
            'action' => 'add',
            'dropdown' => 'post-visibility-menu'
        ];
    } elseif ($type === 'edit') {
        $postForm = [
            'title' => 'EDIT POST',
            'buttonName' => 'SAVE',
            'action' => 'edit',
            'dropdown' => 'post-visibility-menu'
        ];
    } elseif ($type === 'share') {
        $postForm = [
            'title' => 'SHARE POST',
            'buttonName' => 'SHARE',
            'action' => 'add',
            'dropdown' => 'sharepost-visibility-menu'
        ];
    }


    if (!isset($post)) {
        $post = null;
    }

    echo $this->Form->create($post, [
        'url' => ['controller' => 'Posts', 'action' => $postForm['action']],
        'class' => 'mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp sb-post-add-form',
        'style' => 'overflow: visible;',
        'type' => 'file',
        'novalidate' => true,
        'error' => false
    ]);
    ?>
    <!-- <div class="mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp" style="padding: 0px"> -->
    <?= $this->element('component.loader') ?>
    <div class="mdl-card__title sb-card-segment sb-post-add-form__title">
        <?php if ($type === 'share') : ?>
            <button type="button" class="mdl-button mdl-js-button mdl-button--icon close">
                <i class="material-icons">arrow_back_ios</i>
            </button>
        <?php endif; ?>
        <h3 class="mdl-card__title-text"><?= $postForm['title'] ?></h3>
    </div>
    <?php if (isset($errors)) : ?>
        <div class="sb-card-segment mdl-color--red-100 mdl-color-text--red-300" style="padding-right: 40px">
            <i class="material-icons sb-post-add-form__remove-error" style="position: absolute; right: 10px; cursor: pointer">clear</i>
            <p style="margin: 0px">There was an error submitting your post: </p>
            <ul style="margin: 0px">
                <?php foreach ($errors as $field) : ?>
                    <?php foreach ($field as $value) : ?>
                        <li><?= $value ?></li>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="mdl-card__supporting-text sb-card-segment" style="border-bottom: none">
        <?php
        echo $this->Form->control('body', [
            'type' => 'textarea',
            'rows' => 3,
            'maxlength' => 140,
            'placeholder' => 'Write something...',
            'class' => 'sb-textarea post-form-body-field',
            'templates' => [
                'inputContainer' => '{{content}}',
                'inputContainerError' => '{{content}}',
                'label' => false,
                'textarea' => '<textarea name="{{name}}"{{attrs}}>{{value}}</textarea>'
            ],
        ]);
        echo $this->Form->control('visibility', [
            'type' => 'hidden',
            'value' => isset($post['visibility']) ? $post['visibility'] : 0
        ]);
        if($type === 'share') {
            echo $this->Form->control('user_share_id', ['type' => 'hidden']);
            echo $this->Form->control('post_id', ['type' => 'hidden']);
            echo $this->Form->control('original_post_id', ['type' => 'hidden']);
            echo $this->Form->control('original_user_share_id', ['type' => 'hidden']);
        }
        ?>
    </div>
    <div style="width: 100%">
        <div class="sb-color2 progress-counter" style="height:2px;width:0%"></div>
    </div>
    <div class="sb-post-add-form__image-preview sb-card-segment <?= isset($post['image']) ? '' : 'hide-element' ?>">
        <p>Image Preview: </p>
        <div class="mdl-card mdl-shadow--2dp sb-post-add-form__image" style="width:100%; margin: 16px 0px; padding: 16px 35px">
            <?php if (isset($post['image'])) : ?>
                <img class="sb-post-item--display-image" src="<?= str_replace('webroot', '', $post['image_dir']) . $post['image'] ?>">
            <?php endif; ?>
        </div>
    </div>
    <?php
    if (isset($sharePost)) {
        $sharePost['isShare'] = true;
        echo '<div class="sb-card-segment">';
        echo $this->element('item.post', ['post' => $sharePost]);
        echo '</div>';
    }
    ?>
    <div class="mdl-card__actions mdl-card--border">
        <button type="submit" class="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect sb-color1 mdl-color-text--white icon-text-container" style="border-radius: 20px; margin:5px 2px">
            <i class="material-icons" style="font-size:20px">send</i><span style="margin-left: 10px"><?= $postForm['buttonName'] ?></span>
        </button>
        <button type="button" id="<?= $postForm['dropdown'] ?>" class="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect sb-color2 mdl-color-text--white" style="border-radius: 20px; margin:5px 2px">
            <i class="material-icons" style="font-size:20px">remove_red_eye</i>
            <span class="sb-visibility-text" style="margin-left: 10px">
                <?php
                if (isset($post['visibility'])) {
                    switch ($post['visibility']) {
                        case 0:
                            echo 'Public';
                            break;
                        case 1:
                            echo 'Followers';
                            break;
                        case 2:
                            echo 'Only Me';
                            break;
                    }
                } else {
                    echo 'Public';
                }
                ?>
            </span>
        </button>
        <?php
        echo $this->Form->control('image', [
            'type' => 'file',
            'class' => 'sb-post-add-form__file-upload',
            'error' => false,
            'templates' => [
                'label' => '<i class="material-icons" style="font-size:20px">attach_file</i><span style="margin-left: 10px">Attach Image</span>',
                'inputContainer' => '<div class="mdl-button mdl-button--file mdl-button--raised mdl-js-button mdl-js-ripple-effect sb-color1 mdl-color-text--white" style="border-radius: 20px; margin:5px 2px"">{{label}}{{content}}</div>'
            ]
        ]);
        ?>

        <ul class="mdl-menu mdl-menu--top-left mdl-js-menu mdl-js-ripple-effect" for="<?= $postForm['dropdown'] ?>">
            <li class="mdl-menu__item sb-visibility-option" data-visibility="0" data-name="Public">Public</li>
            <li class="mdl-menu__item sb-visibility-option" data-visibility="1" data-name="Followers">Followers</li>
            <li class="mdl-menu__item sb-visibility-option" data-visibility="2" data-name="Only Me">Only Me</li>
        </ul>

    </div>

    <!-- </div> -->
    <?php
    echo $this->Form->end();
    ?>
    <?= $this->Flash->render() ?>