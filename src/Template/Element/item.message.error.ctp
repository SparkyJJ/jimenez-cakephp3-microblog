<div class="mdl-card mdl-shadow--2dp mdl-grid mdl-cell mdl-cell--12-col mdl-color--red-100">
    <div class="mdl-card__title">
        <h3 class="mdl-card__title-text mdl-color-text--red-600" style="font-size: 18px">Unavailable Content. Unable to see post.</h3>
    </div>
</div>