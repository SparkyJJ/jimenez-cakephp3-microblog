<?php if (!empty($comments)) : ?>
    <div class="sb-comment-feed">
        <div class="mdl-grid" style="padding-bottom: 16px">
            <div class="mdl-cell mdl-cell--1-col" style="margin-top:0px; display: flex; justify-content: center;align-items: center;">
                <?= $this->Paginator->prev() ?>
                <!-- // if (!empty($post['Comment'])) {
                    //     echo $this->Paginator->prev(
                    //         '<i class="chevron left icon"></i>',
                    //         [
                    //             'escape' => false,
                    //             'url' => ['controller' => 'comments', 'action' => 'paginated_comments'],
                    //             'class' => 'prev-comments',
                    //             'model' => 'Comment'
                    //         ],
                    //         null,
                    //         array('class' => 'prev disabled')
                    //     );
                    // } -->
            </div>
            <div class="mdl-cell mdl-cell--10-col" style="margin-top:0px">
                <div class="ui threaded comments">

                    <?php
                        foreach ($comments as $comment) {
                            echo $this->element('item.comment', ['comment' => $comment]);
                            // echo $this->element('component.comment.form', ['comment' => $comment, 'post_id' => $comment['post_id']]);
                        }
                        // foreach ($post['Comment'] as $row) {
                        //     if (!isset($row['Comment'])) {
                        //         $row['Comment'] = $row;
                        //         $row['User'] = $row['Comment']['User'];
                        //         $row['Reply'] = $row['Comment']['Reply'];
                        //         unset($row['Comment']['Reply']);
                        //         unset($row['Comment']['User']);
                        //     }
                        //     echo $this->element('item.comment', ['comment' => $row]);
                        // }
                        ?>
                </div>
            </div>
            <div class="mdl-cell mdl-cell--1-col" style="margin-top:0px; display: flex; justify-content: center;align-items: center;">
                <?= $this->Paginator->next() ?>

                <?php
                    // if (!empty($post['Comment'])) {
                    //     echo $this->Paginator->next(
                    //         '<i class="chevron right icon"></i>',
                    //         [
                    //             'escape' => false,
                    //             'url' => ['controller' => 'comments', 'action' => 'paginated_comments'],
                    //             'class' => 'next-comments',
                    //             'model' => 'Comment'
                    //         ],
                    //         null,
                    //         array('class' => 'next disabled')
                    //     );
                    // }
                    ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?= $this->Flash->render() ?>