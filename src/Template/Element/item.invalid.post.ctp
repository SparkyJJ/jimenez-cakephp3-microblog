<div class="item-post-card mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp" data-user-id="<?= h($user['id']) ?>" style="padding: 0px">
    <div class="image-multext-container sb-ajax-link__main" style="color: black; padding: 16px 24px 8px 24px; width: 100%">
        <?= $this->element('SubElement/avatar', ['avatar' => $user]) ?>
        <div class="image-multext-multext">
            <p class="sb-ajax-link__main" style="font-size: 18px; margin: 0px"><?= $this->Html->link($user['first_name'] . ' ' . $user['last_name'], ['controller' => 'Users', 'action' => 'profile', $user['id']], ['style' => 'text-decoration: none;color:inherit;']) ?></p>
            <p class="sb-ajax-link__main" style="color: #999;font-size: 14px; margin: 0px">@<?= $this->Html->link($user['username'], ['controller' => 'Users', 'action' => 'profile', $user['id']], ['style' => 'text-decoration: none;color:inherit;']) ?></p>
        </div>
    </div>
    <div class="mdl-cell mdl-cell--12-col sb-post-item-body" style="width: 100%; padding: 8px 16px!important;">
        <?= $this->element('item.message.error') ?>
    </div>
</div>

<?= $this->Flash->render() ?>