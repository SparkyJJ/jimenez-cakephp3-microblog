<div class="sb-profile-detail mdl-grid hide-element" style="font-size: 14px">
    <div class="mdl-cell mdl-cell--6-col">
        <b> First Name : </b> <?= $user['first_name'] ?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Last Name : </b> <?= $user['last_name'] ?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Gender : </b> <?= $user['gender'] === 0 ? 'Male' : 'Female' ?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Birth Date : </b> <?= $user['birth_date']->format('F d, Y')?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Address : </b> <?= $user['address'] ?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Email : </b> <?= $user['email'] ?>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
        <b>Mobile : </b> <?= $user['mobile_no'] ?>
    </div>
</div>