<?php
if (isset($tabView)) {
    if ($tabView === 'edit') {
        $editActive = 'is-active';
        $changeActive = '';
    } else {
        $changeActive = 'is-active';
        $editActive = '';
    }
} else {
    $editActive = 'is-active';
    $changeActive = '';
}
?>
<div id="profile-card" class="mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp" style="padding: 0px">
    <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
        <div class="mdl-tabs__tab-bar">
            <a href="#edit-profile-panel" class="mdl-tabs__tab mdl-cell--6-col <?= $editActive ?>">EDIT PROFILE</a>
            <a href="#change-password-panel" class="mdl-tabs__tab mdl-cell--6-col <?= $changeActive ?>">CHANGE PASSWORD</a>
        </div>
        <div class="mdl-tabs__panel <?= $editActive ?>" id="edit-profile-panel">
            <?= $this->element('component.profile.form.edit', compact('user')) ?>
        </div>
        <div class="mdl-tabs__panel <?= $changeActive ?>" id="change-password-panel">
            <?= $this->element('component.profile.form.change') ?>
        </div>
    </div>
    <div class="general-loader hide-element">
        <div class="center-element">
            <div class="mdl-spinner mdl-js-spinner is-active"></div>
        </div>
    </div>
</div>