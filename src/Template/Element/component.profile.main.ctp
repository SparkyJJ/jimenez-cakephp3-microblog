<?php
$authUser = $this->request->getSession()->read('Auth.User');
?>
<div class="sb-profile-main">
    <h3 style="margin: 0px"><?= h($user['first_name']) . ' ' . h($user['last_name']) ?></h3>
    <h5 style="margin: 0px; color: #bbb">@<?= h($user['username']) ?></h5>
    <div class=" sb-ajax-link__main" style="margin-top: 15px">
        <?= $this->Html->link(
            '<span class="mdl-chip__text">Followers</span><span class="mdl-chip__text sb-profile-follower-count" style="color:teal; margin-left: 10px">' . h($user['follower_count']) . '</span>',
            [
                'controller' => 'Followers', 'action' => 'followers', $user['id']
            ],
            [
                'escape' => false,
                'class' => 'mdl-chip'
            ]
        ) ?>
        <?= $this->Html->link(
            '<span class="mdl-chip__text">Following</span><span class="mdl-chip__text sb-profile-following-count" style="color:teal; margin-left: 10px">' . h($user['following_count']) . '</span>',
            [
                'controller' => 'Followers', 'action' => 'following', $user['id']
            ],
            [
                'escape' => false,
                'class' => 'mdl-chip'
            ]
        ) ?>
        <?php if ($authUser['id'] !== $user['id']) : ?>
            <?= $this->element('component.follow.button', ['user' => $user]) ?>
        <?php endif; ?>
    </div>
</div>