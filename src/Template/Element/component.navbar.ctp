<!-- The drawer is always open in large screens. The header is always shown,
  even in small screens. -->
<header class="mdl-layout__header sb-color1" style="color: white">
    <div class="mdl-layout__header-row">
        <div class="mdl-layout-spacer"></div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable
                  mdl-textfield--floating-label mdl-textfield--align-right">
            <label class="mdl-button mdl-js-button mdl-button--icon" for="fixed-header-drawer-exp">
                <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
                <input class="mdl-textfield__input" type="text" name="sample" id="fixed-header-drawer-exp">
            </div>
        </div>
    </div>
</header>