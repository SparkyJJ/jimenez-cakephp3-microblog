<?php
$authUser = $this->request->getSession()->read('Auth.User');
?>
<div id="sb-profile-card" class="mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp" style="padding: 0px">
    <div class="general-loader hide-element">
        <div class="center-element">
            <div class="mdl-spinner mdl-js-spinner is-active"></div>
        </div>
    </div>
    <div class="mdl-cell mdl-cell--3-col-desktop mdl-cell--2-col-tablet mdl-cell--12-col-phone mdl-cell--middle" style="padding:15px;">
        <?php
        echo $this->element('SubElement/avatar', [
            'avatar' => $user,
            'externalConfig' => [
                'style' => [
                    'width: 100%;',
                    'border-radius: 50%;',
                ]
            ],
            'size' => 'lg'
        ]);
        ?>
    </div>
    <div class="mdl-cell mdl-cell--9-col-desktop mdl-cell--6-col-tablet mdl-cell--12-col-phone mdl-cell--middle" style="padding: 5px 20px">

        <?php
        echo $this->element('component.profile.detail', ['user' => $user]);
        echo $this->element('component.profile.main', ['user' => $user]);
        ?>
    </div>
    <div class="mdl-cell mdl-cell--12-col" style="text-align: center; margin-top: 0px">
        <label class="mdl-icon-toggle mdl-js-icon-toggle mdl-js-ripple-effect" for="icon-toggle-1">
            <input type="checkbox" id="icon-toggle-1" class="mdl-icon-toggle__input" style="display:none">
            <i class="mdl-icon-toggle__label material-icons">more_horiz</i>
        </label>
    </div>
    <?php if ($authUser['id'] === $user['id']) : ?>
        <div class="mdl-card__menu sb-ajax-link__main">
            <a class="sb-profile-settings-button mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" href="/users/settings">
                <i class="material-icons">settings</i>
            </a>
        </div>
    <?php endif; ?>
</div>