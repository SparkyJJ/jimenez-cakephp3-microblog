<?php
if (!isset($avatar)) {
    $avatar = $this->request->getSession()->read('Auth.User');
}

if (!isset($size)) {
    $size = 'sm';
}

if (isset($avatar['profile_picture'])) {
    $picExt = explode('.', $avatar['profile_picture']);
    $dir = 'Users/profile_picture/' . $avatar['unique_id'] . '/' . $avatar['username'] . "_$size." . end($picExt);
} else {
    $dir = "Users/profile_picture/default/default_$size.png";
}

$imageConfig = [
    'class' => 'image-multext-img',
    'style' => [
        'border-radius: 50%;',
    ],
    'onerror' => 'this.onerror=null;this.src="img/Users/profile_picture/default/default_' . $size . '.png";',
    'url' => ['controller' => 'Users', 'action' => 'profile', $avatar['id']]
];

if (isset($externalConfig)) {
    $imageConfig = $externalConfig;
}

echo $this->Html->image(
    $dir,
    $imageConfig
);
