<?php
if(!isset($isLike)) {
    $isLike = true;
}

$likeConfig = $isLike ? [
    'name' => 'Unlike',
    'action' => 'unlike',
    'class' => 'sb-color--text2',
] : [
    'name' => 'Like',
    'action' => 'like',
    'class' => 'sb-color1 mdl-color-text--white'
];
echo $this->Form->postLink(
    '<i class="material-icons">thumb_up</i>
    <span class="sb-badge-button__text">'. $likeConfig['name'] .'</span>
    <span class="sb-badge-button__badge mdl-color--white sb-color--text1">' . $post['like_count'] . '</span>',
    ['controller' => 'Likes', 'action' => $likeConfig['action'], $post['id']],
    [
        'data' => ['post_id' => $post['id']],
        'class' => 'mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect icon-text-container sb-badge-button sb-like-button ' . $likeConfig['class'],
        'escape' => false
    ]
);
