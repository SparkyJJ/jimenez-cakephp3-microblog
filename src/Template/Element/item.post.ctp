<?php
$authUser = $this->request->getSession()->read('Auth.User');
$rand = rand();
?>
<div class="item-post-card mdl-card mdl-grid mdl-cell mdl-cell--12-col mdl-shadow--2dp" data-post-id="<?= h($post['id']) ?>" data-user-id="<?= h($post['user_id']) ?>" style="padding: 0px">
    <div class="image-multext-container sb-ajax-link__main" style="color: black; padding: 16px 24px 8px 24px; width: 100%">
        <?= $this->element('SubElement/avatar', ['avatar' => $post['user'], 'size' => 'sm']) ?>
        <div class="image-multext-multext">
            <p class="sb-ajax-link__main mdl-color-text--teal-800" style="font-size: 18px; margin: 0px"><?= $this->Html->link($post['user']['first_name'] . ' ' . $post['user']['last_name'], ['controller' => 'Users', 'action' => 'profile', $post['user']['id']], ['style' => 'text-decoration: none;color:inherit;']) ?></p>
            <p class="sb-ajax-link__main" style="color: #999;font-size: 14px; margin: 0px">@<?= $this->Html->link($post['user']['username'], ['controller' => 'Users', 'action' => 'profile', $post['user']['id']], ['style' => 'text-decoration: none;color:inherit;']) ?>
                <i id='tt-visibility-<?= $post['id'] . $rand ?>' class="material-icons" style="color:#777;margin-left: 5px;font-size:14px">remove_red_eye</i>
            </p>
            <div class="mdl-tooltip mdl-tooltip--right" data-mdl-for='tt-visibility-<?= $post['id'] . $rand ?>'>
                <?php
                switch ($post['visibility']) {
                    case 0:
                        echo 'Public';
                        break;
                    case 1:
                        echo 'Followers';
                        break;
                    case 2:
                        echo 'Only Me';
                        break;
                    default:
                        echo 'Public';
                        break;
                }
                ?>
            </div>
        </div>
    </div>
    <div class="mdl-cell mdl-cell--12-col sb-post-item-body" style="width: 100%">
        <!-- BODY -->
        <?= nl2br(h($post['body'])) ?>

        <!-- IMAGE -->
        <?php if (isset($post['image_dir'])) : ?>
            <div class="mdl-card mdl-shadow--2dp" style="width:100%; margin: 16px 0px; padding: 16px">
                <img class="sb-post-item--display-image" src="<?= str_replace('webroot', '', $post['image_dir']) . $post['image'] ?>">
            </div>
        <?php endif; ?>

        <?php
        if (isset($post['share_post'])) {
            $sharePost = $post['share_post'];
            $sharePost['user'] = $post['share_user'];
            $sharePost['isShare'] = true;
            echo $this->element('item.post', ['post' => $sharePost]);
        } elseif (isset($post['original_post'])) {
            $sharePost = $post['original_post'];
            $sharePost['user'] = $post['original_share_user'];
            $sharePost['isShare'] = true;
            echo $this->element('item.post', ['post' => $sharePost]);
        } elseif (isset($post['post_id'])) {
            echo $this->element('item.invalid.post', ['user' => $post['share_user']]);
        }
        ?>

        <!-- DATE POSTED/MODIFIED -->
        <p style="color: #aaa; margin:16px 0px 0px 0px">
            <?= $post['created']->format('F d, Y g:i A') ?>
            <?php if (strtotime($post['created']->format('Y-m-d h:i:s A')) != strtotime($post['modified']->format('Y-m-d h:i:s A'))) : ?>
                <i id="tt-modified-<?= $post['id'] . $rand ?>" class="material-icons" style="color:#aaa;margin-left: 5px;font-size:14px">info</i>
            <?php endif; ?>
        </p>
        <div class="mdl-tooltip mdl-tooltip--right" data-mdl-for="tt-modified-<?= $post['id'] . $rand ?>">
            Modified: <br><?= $post['modified']->format('F d, Y g:i A') ?>
        </div>
    </div>
    <?php if (!isset($post['isShare'])) : ?>

        <div class="mdl-card__actions mdl-card--border" style="padding: 8px 24px;">
            <?php
                $isLike = !empty($post['likes']);
                echo $this->element('component.like.button', compact('post', 'isLike'));
                ?>
            <button type="button" class="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect icon-text-container sb-badge-button sb-comment-button <?= $post['comment_count'] <= 0 ? 'sb-color2 mdl-color-text--white' : 'sb-color--text2' ?>">
                <i class="material-icons">comment</i>
                <span class="sb-badge-button__text">Comment</span>
                <span class="sb-badge-button__badge mdl-color--white sb-color--text1"><?= $post['comment_count'] ?></span>
            </button>
            <button type="button" class="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect sb-color1 mdl-color-text--white icon-text-container sb-badge-button sb-share-post">
                <i class="material-icons">share</i>
                <span class="sb-badge-button__text">Share</span>
                <span class="sb-badge-button__badge mdl-color--white sb-color--text1"><?= $post['post_count'] ?></span>
            </button>
        </div>

        <div class="sb-comment-segment" <?= $post['comment_count'] <= 0 ? 'style="display: none;"' : '' ?>>
            <?= $this->element('component.comment.form', ['post_id' => $post['id']]) ?>
            <?php $cell = $this->cell('Comments', [$post['id'], true]); ?>
            <?= $cell ?>
        </div>


        <div class="mdl-card__menu">
            <button type="button" id="post-menu-btn<?= $post['id'] ?>" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                <i class="material-icons">more_verti</i>
            </button>

            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect sb-ajax-link__main" style="padding:0px" for="post-menu-btn<?= $post['id'] ?>">
                <?= $this->Html->link('View post', ['controller' => 'Posts', 'action' => 'view', $post['id']], ['class' => 'mdl-menu__item']) ?>
                <?php if ($authUser['id'] === $post['user_id']) : ?>
                    <?= $this->Form->postLink('Edit post', ['controller' => 'Posts', 'action' => 'edit', $post['id']], ['class' => 'mdl-menu__item sb-sidemenu-edit sb-ajax-exclude']) ?>
                    <?= $this->Form->postLink('Delete post', ['controller' => 'Posts', 'action' => 'delete', $post['id']], ['class' => 'mdl-menu__item sb-sidemenu-delete sb-ajax-exclude']) ?>
                <?php endif; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>

<?= $this->Flash->render() ?>