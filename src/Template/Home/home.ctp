<div id="home-container" class="mdl-cell mdl-cell--12-col mdl-cell--middle text-center">
    <h1 class="ui inverted header" style="margin-bottom: 0px">
        &nbsp;<span class="mbtitle mdl-color-text--teal-900">SparkBlog</span>&nbsp;
    </h1>
    <h6 class="mbtitle text-center" style="letter-spacing:0.5em; margin: 0px;">Version 2.0</h6>

    <h3>&nbsp;<span class="mbtitle">Do whatever you want with it.</span>&nbsp;</h3>
    <!-- Raised button with ripple -->
    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--teal-400 mdl-color-text--white nav-button" style="width: 150px" data-value="login">
        Login
    </button>
    <!-- Raised button with ripple -->
    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect nav-button" style="width: 150px" data-value="register">
        Register
    </button>
    <script>
        $('.mbtitle').hide().fadeIn(1500)
    </script>
</div>
<?= $this->Flash->render('flash', [
    'element' => 'Flash/toast'
]) ?>