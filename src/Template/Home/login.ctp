<div id="login-container" class="mdl-cell mdl-cell--12-col mdl-cell--middle">
    <div class="mdl-card mdl-cell mdl-cell--4-col mdl-cell--4-offset-desktop mdl-cell--2-offset-tablet mdl-shadow--2dp">
        <div class="mdl-card__title" style="padding-bottom: 0px">
            <button class="mdl-button mdl-js-button mdl-button--icon nav-button" data-value="home">
                <i class="material-icons">arrow_back_ios</i>
            </button>
            <h2 class="mdl-card__title-text">LOGIN TO SPARKBLOG</h2>
        </div>
        <div class="mdl-card__supporting-text mdl-cell mdl-cell--12-col" style="padding-top: 0px">
            <div class="general-loader hide-element">
                <div class="center-element">
                    <div class="mdl-spinner mdl-js-spinner is-active"></div>
                </div>
            </div>
            <?php
            if (!isset($login)) {
                $login = null;
            }
            echo $this->Form->create($login, [
                'url' => ['controller' => 'users', 'action' => 'login'],
                'type' => 'post',
                'novalidate' => true
            ]);
            // The following generates a Text input
            echo $this->Form->control('username', ['templateVars' => ['containerClass' => 'mdl-cell mdl-cell--12-col']]);
            // The following generates a Password input
            echo $this->Form->control('password', ['templateVars' => ['containerClass' => 'mdl-cell mdl-cell--12-col']]);
            echo $this->Form->button('Login', [
                'type' => 'submit',
                'templateVars' => [
                    'buttonClass' => 'mdl-color--teal-400 mdl-color-text--white mdl-cell mdl-cell--12-col'
                ]
            ]);
            echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>
