$('body').on('click', '.nav-button', function() {
    let value = $(this).data('value')
    $('#main-container').children().fadeOut()
    $.ajax({
        url: "/pages/view/" + value,
        success: function(result) {
            let data = JSON.parse(result)
            let element = $(data.content.element)
            $('#main-container').html(element)
            $(element).hide().fadeIn()
            componentHandler.upgradeDom()
            $('#birth-date').datepicker({
                maxDate: new Date()
            })
        },
        error: function() {
            $('#main-container').children().fadeIn()
        }
    })
})

$('body').on('submit', '#register-container form', function(e) {
    let form = $(this)
    let url = form.attr('action')
    e.preventDefault()
    let formData = new FormData(this)

    $('.general-loader').toggleClass('hide-element')
    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        url: url,
        data: formData,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result)
            let element = $(data.content.element)
            $('#main-container').html(element)
            componentHandler.upgradeDom()
            $('#birth-date').datepicker({
                maxDate: new Date()
            })
        },
        error: function() {
            $('.general-loader').toggleClass('hide-element')
        }
    })
})

$('body').on('submit', '#login-container form', function(e) {
    let form = $(this)
    let url = form.attr('action')
    e.preventDefault()

    $('.general-loader').toggleClass('hide-element')
    $.ajax({
        type: 'POST',
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result)
            if (data.content !== undefined) {
                let element = $(data.content.element)
                $('#main-container').html(element)
                componentHandler.upgradeDom()
            } else {
                window.location.replace(data.redirectUrl)
            }
        },
        error: function() {
            $('.general-loader').toggleClass('hide-element')
        }
    })
})

$('body').on('change', 'input[type="file"]', function() {
    let name = ''
    if (this.files && this.files[0]) {
        name = this.files[0].name;
        var reader = new FileReader();

        reader.onload = function(e) {
            let image = '<img src=' + e.target.result + ' class="shadow-element" style="max-height:100px; max-width:100%;">'
            $('#preview-image').html(image)
        }

        reader.readAsDataURL(this.files[0]);
    } else {
        $('#preview-image').html('')
    }
    $(this).parent().siblings('input').val(name);
})