//================================================================//
//=========================== ADD POST ===========================//
//================================================================//
$('body').on('submit', 'form.sb-post-add-form', function(e) {
    let form = $(this);
    let url = form.attr('action');
    e.preventDefault();
    let formData = new FormData(this);
    let loader = $(this).children('.general-loader');
    loader.toggleClass('hide-element');

    ajaxRequestExecute({
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        url: url,
        data: formData,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result);
            let element = $(data.content.element);
            if (data.status === undefined) {
                element.find('textarea').val('');
                $('#sb-main-view').html(element);
                if ($('#general-modal').attr('open') === 'open') {
                    $('#general-modal .close').click();
                }
                $('#sb-main-view').find('.item-post-card').hide().transition('fade down', { duration: 1000 });
            } else {
                if (data.type === 'share') {
                    element.toggleClass('sb-rmm');
                }
                form.replaceWith(element);
            }
            componentHandler.upgradeDom();
            initializeNewView();
        },
        error: function() {
            loader.toggleClass('hide-element');
        }
    })
})

//===================================================================//
//=========================== DELETE POST ===========================//
//===================================================================//
$('body').on('click', '.sb-delete-post', function() {
    let button = $(this);
    let name = $(this).data('form');
    let form = $('form[name=' + name + ']');
    let url = form.attr('action');
    button.removeClass('sb-delete-post');
    ajaxRequestExecute({
        type: 'POST',
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            $('#general-modal')[0].close();
            let data = JSON.parse(result);
            let element = $(data.content.element);
            element.find('textarea').val('');
            $('#sb-main-view').html(element);
            $('#sb-main-view').find('.item-post-card').hide().transition('fade down', { duration: 1000 });
            $('#general-modal .close').click();

            initializeNewView();

            componentHandler.upgradeDom();
        }
    })
})

//===================================================================//
//=========================== EDIT POST ===========================//
//===================================================================//
$('body').on('click', '.sb-sidemenu-edit', function() {
    let button = $(this);
    let form = button.prev('form');
    let url = form.attr('action');
    button.removeClass('sb-sidemenu-edit');
    ajaxRequestExecute({
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            $('#general-modal')[0].close();
            let data = JSON.parse(result);
            let element = $(data.content.element);
            updateView(element, '#sb-main-view');
        }
    })
})

//===================================================================//
//=========================== SHARE MODAL ===========================//
//===================================================================//
$('body').on('click', '.sb-share-post', function() {
    let postid = $(this).closest('.item-post-card').data('post-id')
    let userid = $(this).closest('.item-post-card').data('user-id')
    let name = $(this).data('form');
    let form = $('form[name=' + name + ']');
    let url = form.attr('action');
    ajaxRequestExecute({
        url: '/posts/view_share/' + postid + '/' + userid,
        success: function(result) {
            let data = JSON.parse(result);
            let element = $(data.content.element);
            $('#general-modal').html(element);
            $('#general-modal').toggleClass('sb-rmp');
            element.toggleClass('sb-rmm');
            $('#general-modal')[0].showModal();
            $('#general-modal').hide().transition('scale');
            dialogPolyfill.reposition($('#general-modal')[0]);
            componentHandler.upgradeDom();
        }
    })
})

//=========================================================================//
//=========================== VISIBILITY SELECT ===========================//
//=========================================================================//
$('body').on('click', '.sb-visibility-option', function() {
    let option = $(this);
    let form = option.closest('form');
    form.find('input[name="visibility"]').val(parseInt(option.data('visibility')));
    form.find('.sb-visibility-text').html(option.data('name'));
})

//=====================================================================//
//=========================== IMAGE PREVIEW ===========================//
//=====================================================================//
$('body').on('change', 'input[type="file"].sb-post-add-form__file-upload', function() {

    let form = $(this).closest('.sb-post-add-form')
    if (this.files && this.files[0]) {
        let uploadedFile = this.files[0];
        if (uploadedFile.size >= 5000000) {
            form.find('.sb-post-add-form__image').empty();
            form.find('.sb-post-add-form__image-preview').addClass('hide-element');
            form.find('input[type="file"].sb-post-add-form__file-upload').val('');
            if (form.closest('#general-modal').length > 0) {
                dialogPolyfill.reposition($('#general-modal')[0]);
            }
            r(function() {
                let toast_data = {
                    message: '<i class="material-icons mdl-color-text--red-500">error_outline</i>&nbsp;File is too large to upload.',
                };
                $('#general-snackbar')[0].MaterialSnackbar.showSnackbar(toast_data);
            });

            function r(f) {
                /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
            }
        } else {
            var reader = new FileReader();

            reader.onload = function(e) {
                form.find('.sb-post-add-form__image-preview').removeClass('hide-element');
                let image = '<i class="material-icons sb-post-add-form__remove-preview" style="position: absolute; top: 5px; left: 5px; z-index: 1; cursor: pointer">clear</i><img class="sb-post-item--display-image" src=' + e.target.result + '>';
                form.find('.sb-post-add-form__image').html(image);
                if (form.parent('#general-modal').length > 0) {
                    dialogPolyfill.reposition($('#general-modal')[0]);
                }
            }

            reader.readAsDataURL(this.files[0]);
        }
    } else {
        form.find('.sb-post-add-form__image-preview').addClass('hide-element');
        // $('#preview-image').html('')
    }
    $(this).parent().siblings('input').val(name);
})

//======================================================================//
//=========================== REMOVE PREVIEW ===========================//
//======================================================================//
$('body').on('click', '.sb-post-add-form__remove-preview', function() {
    let form = $(this).closest('.sb-post-add-form');
    form.find('.sb-post-add-form__image').empty();
    form.find('.sb-post-add-form__image-preview').addClass('hide-element');
    form.find('input[type="file"].sb-post-add-form__file-upload').val('');
    if (form.closest('#general-modal').length > 0) {
        dialogPolyfill.reposition($('#general-modal')[0]);
    }
})

//============================================================================//
//=========================== REMOVE ERROR MESSAGE ===========================//
//============================================================================//
$('body').on('click', '.sb-post-add-form__remove-error', function() {
    $(this).parent().fadeOut();
})

//====================================================================//
//=========================== DELETE MODAL ===========================//
//====================================================================//
$('body').on('click', '.sb-sidemenu-delete', function(e) {
    e.preventDefault();
    $('#general-modal')[0].showModal();
    $('#general-modal').hide().transition('scale');
    dialogPolyfill.reposition($('#general-modal')[0]);
    $('#general-modal').html(
        '<h4 class="mdl-dialog__title mdl-color-text--yellow-800 text-center"><i class="material-icons" style="font-size: 64px">warning</i></h4>' +
        '<div class="mdl-dialog__content">' +
        '<h5 class="text-center" style="margin:0px"> Are you sure you want to delete this post?</h5></div>' +
        '<div class = "mdl-dialog__actions" ><button type="button" class="mdl-button mdl-color-text--green-800 sb-delete-post" data-form="' +
        $(this).prev('form').attr('name') +
        '"> Agree </button>' +
        '<button type = "button" class = "mdl-button close mdl-color-text--red-800"> Disagree </button> </div>'
    );
})

//===================================================================//
//=========================== LIKE POST ===========================//
//===================================================================//
$('body').on('click', '.sb-like-button', function() {
    let button = $(this);
    let form = $(this).siblings('form');
    let url = form.attr('action');
    button.removeClass('sb-like-button');
    ajaxRequestExecute({
        type: 'POST',
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            // $('#general-modal')[0].close();
            let data = JSON.parse(result);
            let element = $(data.content.element);
            button.prev().remove();
            button.replaceWith(element);
            initializeNewView();
            button.addClass('sb-like-button');
            // element.find('textarea').val('');
            // $('#sb-main-view').html(element);
            // sb_fadeDown($('.sb-main-feed'));
            // initializeNewView();

            componentHandler.upgradeDom();
        },
        error: function() {
            button.addClass('sb-like-button');
        }
    })
})

//===================================================================//
//=========================== COMMENT BUTTON ===========================//
//===================================================================//
$('body').on('click', '.sb-comment-button', function() {
    let button = $(this);
    button.removeClass('sb-comment-button');
    button.closest('.item-post-card').find('.sb-comment-segment').transition('drop', {
        onComplete: function() {
            button.addClass('sb-comment-button');
        }
    });
    button.toggleClass('sb-color2 mdl-color-text--white sb-color--text2');
})