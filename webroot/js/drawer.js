setInterval(function() {
    ajaxRequestExecute({
        sbName: 'getFollowDrawer',
        headers: { Accept: "application/json" },
        contentType: "application/json",
        url: "/followers/get_count",
        success: function(result) {
            let data = result;
            $('.sb-drawer-follower-count').attr('data-badge', data.follow.follower_count);
            $('.sb-drawer-following-count').attr('data-badge', data.follow.following_count);
            $('.sb-drawer-requests-count').attr('data-badge', data.follow.follow_request_count);
        },
    });
}, 10000);

function abortAll() {
    var calls = Array.from($.xhrPool);

    $.each(calls, function(key, value) {
        value.abort();
    });
}

$('body').on('click', '.mdl-layout__subdrawer-button', function() {
    $('.mdl-layout__subdrawer').toggleClass('is-visible')
    $('.mdl-layout__subobfuscator').addClass('is-visible')
})

$('body').on('click', '.mdl-layout__subobfuscator.is-visible', function() {
    $('.mdl-layout__subdrawer').toggleClass('is-visible')
    $('.mdl-layout__subobfuscator').removeClass('is-visible')
})

$('body').on('submit', '.sb-search-bar', function(e) {
    e.preventDefault();
    form = $(this);
    let url = $(this).attr('action');
    ajaxRequestExecute({
        type: 'POST',
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result);
            let element = $(data.content.element);
            if (form.parent().children('.sb-search-list-container').length > 0) {
                form.parent().children('.sb-search-list-container').replaceWith(element);
            } else {
                form.parent().append(element);
            }
        },
        error: function() {
            // loader.toggleClass('hide-element');
        }
    })
})

$('body').on('click', '.sb-search-paginate', function(e) {
    e.preventDefault();
    let drawer = $(this).closest('.mdl-layout__subdrawer');
    let url = $(this).attr('href');
    ajaxRequestExecute({
        url: url,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result);
            let element = $(data.content.element);
            if (drawer.children('.sb-search-list-container').length > 0) {
                drawer.children('.sb-search-list-container').replaceWith(element);
            } else {
                drawer.append(element);
            }
        },
        error: function() {
            // loader.toggleClass('hide-element');
        }
    })
})

$('body').on('click', '.mdl-list__item.mdl-list__item--two-line', function() {
    ajaxChangeView($(this).children('a'), '#sb-main-view', '.main-view-loader');
})