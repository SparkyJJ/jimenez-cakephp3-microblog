$('body').on('submit', '#sb-profile-edit-container form', function(e) {
    let form = $(this)
    let url = form.attr('action')
    e.preventDefault()
    let formData = new FormData(this)
    let loader = $(this).siblings('.general-loader')
    loader.toggleClass('hide-element')
    ajaxRequestExecute({
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        url: url,
        data: formData,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result)
            let element = $(data.content.element)
            let drawer = $(data.content.drawer)
            $('#sb-profile-edit-container').replaceWith(element)
            $('#sb-main-drawer').replaceWith(drawer)
            $(element).parent().children('*:not(script)').hide().fadeIn()
            componentHandler.upgradeDom()
        },
        error: function() {
            loader.toggleClass('hide-element')
        }
    })
})

$('body').on('submit', '#sb-profile-change-container form', function(e) {
    let form = $(this)
    let url = form.attr('action')
    e.preventDefault()
    let loader = $(this).siblings('.general-loader')
    loader.toggleClass('hide-element')
    ajaxRequestExecute({
        type: 'POST',
        cache: false,
        url: url,
        data: form.serialize(),
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
        },
        success: function(result) {
            let data = JSON.parse(result)
            let element = $(data.content.element)
            $('#sb-profile-change-container').replaceWith(element)
            $(element).parent().children('*:not(script)').hide().fadeIn()
            componentHandler.upgradeDom()
        },
        error: function() {
            loader.toggleClass('hide-element')
        }
    })
})

$('body').on('change', 'input[type="file"].sb-profile-edit-container__file-upload', function() {
    let name = ''
    if (this.files && this.files[0]) {
        name = this.files[0].name;
        var reader = new FileReader();

        reader.onload = function(e) {
            let image = '<img src=' + e.target.result + ' class="shadow-element" style="max-height:100px; max-width:100%;">'
            $('#sb-profile-edit-container__preview-image').html(image)
        }

        reader.readAsDataURL(this.files[0]);
    } else {
        $('#sb-profile-edit-container__preview-image').html('')
    }
    $(this).parent().siblings('input').val(name);
})