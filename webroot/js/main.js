let ajaxQueue = {
    request: [],
    requestName: [],
    flag: false
};

let ajaxLinkState = false;

/**
 * AJAX Request Execute Enqueue Function
 * 
 * @param {*} data 
 * @param {*} stackable 
 */

function ajaxRequestExecute(data, stackable = false) {
    if (!ajaxQueue.requestName.includes(data.sbName) || stackable) {
        ajaxQueue.requestName.push(data.sbName);
        ajaxQueue.request.push(data);
        if (!ajaxQueue.flag) {
            ajaxQueue.flag = true;
            ajaxRequestNext();
        }
    }
}


/**
 * AJAX Request Next - Execute next AJAX queue
 */

function ajaxRequestNext() {
    if (ajaxQueue.request.length && ajaxQueue.flag) {
        ajaxQueue.requestName.splice(ajaxQueue.requestName.indexOf(ajaxQueue.request[0].sbName), 1);
        delete ajaxQueue.request[0].sbName;
        $.ajax(ajaxQueue.request.shift()).then(ajaxRequestNext);
    } else {
        ajaxQueue.flag = false;
    }
}

/**
 * On AJAX Request Complete Global Function
 * 
 * Refreshes the page when the authenticated user session expired or
 * there is a forbidden request
 */
$(document).ajaxComplete(function(event, xhr, settings) {
    if (xhr.getResponseHeader('Requires-Auth') === '1' || xhr.status === 403) {
        // if (xhr.getResponseHeader('Requires-Auth') === '1') {
        r(function() {
            let toast_data = {
                message: '<i class="material-icons mdl-color-text--red-500">error_outline</i>&nbsp;Unable to process request.',
            };
            $('#general-snackbar')[0].MaterialSnackbar.showSnackbar(toast_data);
        });

        function r(f) {
            /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
        }
        window.location.reload(false);
    }
});

$(document).ready(function() {
    initializeNewView();
    if (!$('#general-modal')[0].showModal) {
        dialogPolyfill.registerDialog($('#general-modal')[0]);
    }

    /**
     * Modal Close Event Listener
     */
    $('body').on('click', '#general-modal .close', function() {
        $('#general-modal').transition('scale', {
            onComplete: function() {
                $('#general-modal')[0].close();
                $('#general-modal').removeClass('sb-rmp');
            }
        });
    })

    /**
     * View Changer Event Listener On Anchor Tags
     */
    $('body').on('click', '.sb-ajax-link__main>a:not(.sb-ajax-exclude):not([disabled])', function(e) {
        e.preventDefault();
        if (!ajaxLinkState) {
            ajaxLinkState = true;
        } else {
            return false;
        }
        ajaxChangeView($(this), '#sb-main-view', '.main-view-loader');
    })

    /**
     * Custom progress bar for post character count
     */
    $('body').on('keyup', '.post-form-body-field', function() {
        $('.progress-counter').css('width', (($(this).val().length / 140) * 100) + '%')
    })

    /**
     * Toggle Profile Page User Information
     */
    $('body').on('click', '#sb-profile-card input[type=checkbox]', function() {
        let toggleProfile = $(this)
        let toggleButton = $(this).parent()
        toggleProfile.prop('disabled', true);
        if (toggleProfile.is(":checked")) {
            $('.sb-profile-main').fadeOut(300, 'linear', function() {
                $('.sb-profile-detail').fadeIn(300, 'linear', function() {
                    toggleProfile.prop('disabled', false);
                    toggleButton.removeClass('is-disabled')
                }).css("display", "flex");
            })
        } else {
            $('.sb-profile-detail').fadeOut(300, 'linear', function() {
                $('.sb-profile-main').fadeIn(300, 'linear', function() {
                    toggleProfile.prop('disabled', false);
                    toggleButton.removeClass('is-disabled')
                }).css("display", "block");
            })
        }
    })

    /**
     * Follow Request Accept Event Listener
     */
    $('body').on('click', '.sb-user-request-action', function(e) {
        let button = $(this);
        let form = $(this).prev('form');
        let url = form.attr('action');
        let card = button.closest('.sb-item-user-card');
        let loader = card.children('.general-loader');
        loader.toggleClass('hide-element');
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                loader.toggleClass('hide-element');
                // card.remove();
                let data = JSON.parse(result);
                let element = $(data.content.element);
                $('.sb-drawer-follower-count').attr('data-badge', data.authUser_count.follower_count);
                $('.sb-drawer-following-count').attr('data-badge', data.authUser_count.following_count);
                $('.sb-drawer-requests-count').attr('data-badge', data.authUser_count.follow_request_count);
                $('.sb-profile-follower-count').html(data.authUser_count.follower_count);
                $('.sb-profile-following-count').html(data.authUser_count.following_count);
                updateView(element, '#sb-main-view');
            },
            error: function() {
                loader.toggleClass('hide-element');
            }
        })
    })

    /**
     * Follow/Unfollow Button Event Listener
     */
    $('body').on('click', '.sb-follow-button', function(e) {
        let button = $(this);
        let form = $(this).siblings('form');
        let url = form.attr('action');
        button.removeClass('sb-follow-button');
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                // $('#general-modal')[0].close();
                let data = JSON.parse(result);
                let element = $(data.content.element);
                button.prev().remove();
                button.replaceWith(element);
                initializeNewView();
                button.addClass('sb-follow-button');
                $('.sb-drawer-follower-count').attr('data-badge', data.authUser_count.follower_count);
                $('.sb-drawer-following-count').attr('data-badge', data.authUser_count.following_count);
                $('.sb-drawer-requests-count').attr('data-badge', data.authUser_count.follow_request_count);
                $('.sb-profile-follower-count').html(data.user_count.follower_count);
                $('.sb-profile-following-count').html(data.user_count.following_count);

                componentHandler.upgradeDom();
            },
            error: function() {
                button.addClass('sb-like-button');
            }
        })
    })

    $('body').on('click', '.sb-paginate-link', function(e) {
        e.preventDefault();
    })

})

/**
 * AJAX Change View
 * 
 * Changes the view based on the redirect link of the anchor tag through
 * AJAX Requesting the view file
 * 
 * @param {*} anchor 
 * @param {*} view 
 * @param {*} loader 
 */
function ajaxChangeView(anchor, view, loader = null) {
    let url = anchor.attr('href')
    if (loader !== null) {
        $(loader).toggleClass('hide-element')
    }
    $(view).children().fadeOut()
    ajaxRequestExecute({
        // ajaxRequestExecute({
        sbName: 'ajaxChangeView',
        url: url,
        success: function(result) {
            let data = JSON.parse(result)
            let element = $(data.content.element)
            if (loader !== null) {
                $(loader).toggleClass('hide-element');
            }
            updateView(element, view);
        },
        error: function() {
            if (loader !== null) {
                $(loader).toggleClass('hide-element')
            }
            $(view).children().fadeIn()
            ajaxLinkState = false;
        }
    })
}

/**
 * Update View
 * 
 * Actual function that replaces the view based on the element given
 * 
 * @param {*} element 
 * @param {*} view 
 * @param {*} loader 
 */
function updateView(element, view, loader = null) {
    $(view).html(element);
    $(element).parent().children('*:not(script)').hide().transition('fade', {
        duration: 800
    });
    ajaxLinkState = false;
    componentHandler.upgradeDom();
    initializeNewView();
}

function sb_fadeDown(element) {
    element.css({ opacity: 0, top: '-35px', position: 'relative' })
        .animate({ opacity: 1, top: '0px', position: 'static' }, { queue: false, duration: 'slow' });
}

function initializeNewView() {
    $('.sb-sidemenu-delete, .sb-sidemenu-edit, .sb-like-button, .sb-follow-button, .sb-postLink').removeAttr('onclick');
    $('.sb-sidemenu-delete, .sb-sidemenu-edit, .sb-like-button, .sb-follow-button, .sb-postLink').removeAttr('href');
}