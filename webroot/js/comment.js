$(document).ready(function() {
    $('body').on('click', '.sb-comment-form-button', function() {
        $(this).siblings('.sb-comment-form-container').children('form').submit();
    })

    /**
     * Add Comment Form Event Listener
     */
    $('body').on('submit', '.sb-comment-form', function(e) {
        let form = $(this);
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let post = segment.closest('.item-post-card');
        let post_id = post.data('post-id');
        e.preventDefault();
        let formData = new FormData(this);
        let loader = $(this).parent().siblings('.general-loader');
        loader.toggleClass('hide-element');
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                let data = JSON.parse(result);
                let element = $(data.content.element);
                let feed = $(data.content.feed);
                element.find('input[name="comment"]').val('');
                // loader.toggleClass('hide-element');
                // segment.children('.sb-comment-feed').remove();
                // segment.append(feed);
                segment.children('.sb-comment-form-segment').replaceWith(element);
                // segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                post.find('.sb-comment-button .sb-badge-button__badge').html(data.counters.comment_count)
                ajaxRequestExecute({
                    url: '/comments/paginated_comments/' + post_id,
                    success: function(result) {
                        let feed = $(result);
                        segment.children('.sb-comment-feed').remove();
                        segment.append(feed);
                        segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                        loader.toggleClass('hide-element');
                        componentHandler.upgradeDom();
                        initializeNewView();
                    }
                });

                componentHandler.upgradeDom();
                initializeNewView();
            },
            error: function() {
                loader.toggleClass('hide-element');
            }
        })
    })

    /**
     * Edit Comment Form Event Listener
     */
    $('body').on('submit', '.sb-comment-edit-form', function(e) {
        let form = $(this);
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let post = segment.closest('.item-post-card');
        let comment = form.closest('.post-comment');
        let loader = $(this).siblings('.general-loader');
        loader.toggleClass('hide-element');
        e.preventDefault();
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                let data = JSON.parse(result);
                let element = $(data.content.element);
                comment.replaceWith(element);
                // element.parent().hide().transition('drop');

                componentHandler.upgradeDom();
                initializeNewView();
            },
            error: function() {
                loader.toggleClass('hide-element');
            }
        });
    });

    /**
     * Add Reply Comment Form Event Listener
     */
    $('body').on('submit', '.sb-comment-reply-form', function(e) {
        e.preventDefault();
        let form = $(this);
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let post = segment.closest('.item-post-card');
        let comment = form.closest('.post-comment');
        let loader = $(this).siblings('.general-loader');
        let post_id = post.data('post-id');
        // let page_query = segment.find('.sb-comment-paginate').last().attr('href');
        // let page_num = 1;
        // console.log(page_query);
        // let temp = page_query.split('page=');
        // if (temp[1] !== undefined) {
        //     temp = temp[1].split('&');
        //     page_num = parseInt(temp[0]) + 1;
        // }
        // loader.toggleClass('hide-element');
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                let data = JSON.parse(result);
                let element = $(data.content.element);
                let feed = $(data.content.feed);

                element.find('input[name="comment"]').val('');
                segment.children('.sb-comment-form-segment').replaceWith(element);
                post.find('.sb-comment-button .sb-badge-button__badge').html(data.counters.comment_count)
                ajaxRequestExecute({
                    url: '/comments/paginated_comments/' + post_id,
                    success: function(result) {
                        let feed = $(result);
                        segment.children('.sb-comment-feed').remove();
                        segment.append(feed);
                        segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                        // loader.toggleClass('hide-element');
                        componentHandler.upgradeDom();
                        initializeNewView();
                    }
                });

                componentHandler.upgradeDom();
                initializeNewView();
            },
            error: function() {
                loader.toggleClass('hide-element');
            }
        });
    });

    /**
     * Delete Comment Form Event Listener
     */
    $('body').on('click', '.sb-comment-delete', function(e) {
        let button = $(this);
        let form = $(this).prev('form');
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let post = segment.closest('.item-post-card');
        let post_id = post.data('post-id');

        button.removeClass('sb-comment-delete');
        let loader = segment.find('.general-loader');
        loader.toggleClass('hide-element');
        ajaxRequestExecute({
            type: 'POST',
            url: url,
            data: form.serialize(),
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
            },
            success: function(result) {
                let data = JSON.parse(result);
                let element = $(data.content.element);
                // let feed = $(data.content.feed);
                // loader.toggleClass('hide-element');
                // segment.children('.sb-comment-feed').remove();
                // segment.append(feed);
                segment.children('.sb-comment-form-segment').replaceWith(element);
                // segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                post.find('.sb-comment-button .sb-badge-button__badge').html(data.counters.comment_count)
                ajaxRequestExecute({
                    url: '/comments/paginated_comments/' + post_id,
                    success: function(result) {
                        let feed = $(result);
                        loader.toggleClass('hide-element');
                        segment.children('.sb-comment-feed').remove();
                        segment.append(feed);
                        segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                        // post.find('.sb-comment-button .sb-badge-button__badge').html(data.counters.comment_count)
                        componentHandler.upgradeDom();
                        initializeNewView();
                    }
                });
                componentHandler.upgradeDom();
                initializeNewView();
            },
            error: function() {
                loader.toggleClass('hide-element');
                button.addClass('sb-comment-delete');
            }
        })
    })

    /**
     * Display Edit Comment Form Event Listener
     */
    $('body').on('click', '.sb-comment-edit', function(e) {
        let button = $(this);
        let form = $(this).prev('form');
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let post = segment.closest('.item-post-card');
        let reply = button.parent().siblings('.sb-reply-container');
        button.removeClass('sb-comment-edit');
        if (reply.length > 0) {
            if (reply.hasClass('visible')) {
                reply.transition('drop', {
                    onComplete: function() {
                        button.parent().siblings('.comment-text').transition('fade', {
                            onComplete: function() {
                                button.addClass('sb-comment-edit');
                            }
                        })
                    }
                })
            } else {
                button.parent().siblings('.comment-text').transition('fade', {
                    onComplete: function() {
                        reply.transition('drop', {
                            onComplete: function() {
                                button.addClass('sb-comment-edit');
                            }
                        })
                    }
                })
            }
        } else {
            ajaxRequestExecute({
                type: 'POST',
                url: url,
                data: form.serialize(),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
                },
                success: function(result) {
                    let data = JSON.parse(result);
                    let element = $(data.content.element);
                    button.parent().siblings('.comment-text').hide();
                    button.parent().siblings('.comment-text').after(element);
                    // button.parent().siblings('.comment-text').transition('drop');
                    element.hide().transition('fade', {
                        onComplete: function() {
                            button.addClass('sb-comment-edit');
                        }
                    });
                    componentHandler.upgradeDom();
                    initializeNewView();
                },
                error: function() {
                    button.addClass('sb-comment-edit');
                }
            })
        }
    })

    /**
     * Display Reply Comment Form Event Listener
     */
    $('body').on('click', '.sb-comment-reply', function(e) {
        let button = $(this);
        let form = $(this).prev('form');
        let url = form.attr('action');
        let segment = form.closest('.sb-comment-segment');
        let comment = form.closest('.post-comment');
        let post = segment.closest('.item-post-card');
        button.removeClass('sb-comment-reply');
        let reply = button.parent().parent().siblings('.sb-reply-container');
        if (reply.length > 0) {
            reply.transition('drop', {
                onComplete: function() {
                    button.addClass('sb-comment-reply');
                }
            });
        } else {
            ajaxRequestExecute({
                type: 'POST',
                url: url,
                data: form.serialize(),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', form.find('input[name="_csrfToken').val());
                },
                success: function(result) {
                    let data = JSON.parse(result);
                    let element = $(data.content.element);
                    comment.append(element);
                    element.hide().transition('drop', {
                        onComplete: function() {
                            button.addClass('sb-comment-reply');
                        }
                    })
                    componentHandler.upgradeDom();
                    initializeNewView();
                },
                error: function() {
                    button.addClass('sb-comment-reply');
                }
            })
        }
    })

    /**
     * Comment Feed Pagination Event Listener
     */
    $('body').on('click', '.sb-comment-paginate', function(e) {
        e.preventDefault();
        let post_id = $(this).closest('.item-post-card').data('post-id');
        let href = $(this).attr('href');
        let query = href.split('?')[1];
        let segment = $(this).closest('.sb-comment-segment');
        ajaxRequestExecute({
            url: '/comments/paginated_comments/' + post_id + '?' + query,
            success: function(result) {
                let feed = $(result);
                segment.children('.sb-comment-feed').remove();
                segment.append(feed);
                segment.children('.sb-comment-feed').hide().transition('fade down', '800ms');
                componentHandler.upgradeDom();
                initializeNewView();
            }
        });
    })
})