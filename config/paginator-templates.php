<?php
return [
    'nextActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_forward_ios</i></a>',
    'nextDisabled' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect" style="min-width: 0px" disabled><i class="material-icons">arrow_forward_ios</i></a>',
    'prevActive' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2" style="min-width: 0px" href="{{url}}"><i class="material-icons">arrow_back_ios</i></a>',
    'prevDisabled' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect" style="min-width: 0px" disabled><i class="material-icons">arrow_back_ios</i></a>',
    'counterRange',
    'counterPages',
    'first',
    'last',
    'number' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect sb-color--text2 sb-paginate-link" style="min-width: 0px" href="{{url}}">{{text}}</a>',
    'current' => '<a class="mdl-button mdl-js-button mdl-js-ripple-effect" style="min-width: 0px" disabled>{{text}}</a>',
    'ellipsis',
    'sort',
    'sortAsc',
    'sortDesc'
];
