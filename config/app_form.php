<?php
return [
    'inputContainer' => '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{containerClass}}" {{containerStyle}}>{{content}}</div>',
    'input' => '<input type="{{type}}" class="mdl-textfield__input {{inputClass}}" name="{{name}}"{{attrs}}/>',
    'label' => '<label class="mdl-textfield__label"{{attrs}}>{{text}}</label>',
    'inputContainerError' => '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-invalid {{containerClass}}" {{containerStyle}}>{{content}}<span class="mdl-textfield__error {{type}}{{required}} error">{{error}}</span></div>',
    'radio' => '<input class="mdl-radio__button" type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    'nestingLabel' => '{{hidden}}<label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" {{attrs}} style="margin:4px 20px 0px 20px">{{input}}{{text}}</label>',
    'select' => '<select class="mdl-textfield__input {{selectClass}}" name="{{name}}"{{attrs}}>{{content}}</select>',
    'button' => '<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect {{buttonClass}}" {{attrs}}>{{text}}</button>',
    'file' => '<input type="file" name="{{name}}"{{attrs}}>',
    'errorList' => '{{content}}',
    'errorItem' => '{{text}}<br>',
    'textarea' => '<textarea class="mdl-textfield__input" name="{{name}}"{{attrs}}>{{value}}</textarea>'
];
